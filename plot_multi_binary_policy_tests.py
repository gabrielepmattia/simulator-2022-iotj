#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sqlite3

import os
import matplotlib.pyplot as plt
import numpy as np

from log import Log
from plot import PlotUtils

MODULE = "RunMultiBinaryPolicyTests"

PLOT_DIR = "_plots"
LOG_DIR = "_log/no-learning/PWR_2_THRESHOLD/_MULTI_RUNS"
SESSION_ID = "20211007-101904"

db = sqlite3.connect(f"{LOG_DIR}/{SESSION_ID}.db")
cur = db.cursor()

os.makedirs("_plots", exist_ok=True)

policies_i = []
policies_str = []
perc_in_deadline = []
perc_rejected = []
perc_executed = []
perc_forwarded = []
total_jobs = []

res = cur.execute("select *, cast(total_reward as real) / cast(total_jobs as real) as f  from simulations order by f desc")
for line in res:
    policies_i.append(line[0])
    policies_str.append(line[1])
    perc_rejected.append(100 * line[4] / line[5])
    perc_executed.append(100 * line[3] / line[5])
    perc_forwarded.append(100 * line[6] / line[5])
    perc_in_deadline.append(100 * line[8])
    total_jobs.append(line[5])

labels = policies_str

PlotUtils.use_tex()

x = np.arange(len(labels))  # the label locations
width = 0.30  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width, perc_in_deadline, width, label='InDeadline')
rects2 = ax.bar(x, perc_rejected, width, label='Rejected')
rects3 = ax.bar(x + width, perc_forwarded, width, label='Forwarded')


# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('% of jobs / total')
# ax.set_title(r"$\rho$ = 0.75, 6 states, 6 nodes, deadline = $0.043s$, job\_duration = $0.020s$")
ax.set_xticks(x)
ax.set_xticklabels(labels, rotation="vertical")
ax.margins(0.01)
ax.legend()

ax.set_ylabel("Percentage of Tasks (\%)")
ax.set_xlabel("Scheduling Policy")
ax.set_ylim([0, 100])

# ax.bar_label(rects1, padding=3)
# ax.bar_label(rects2, padding=3)

fig.tight_layout()
fig.set_figwidth(10)  # 6.4
fig.set_figheight(3)

fig.subplots_adjust(
    top=0.959,
    bottom=0.24,
    left=0.06,
    right=0.993,
    hspace=0.2,
    wspace=0.2
)

# plt.show()

print(perc_in_deadline)

Log.minfo(MODULE, f"Plotting to {PLOT_DIR}/bar_multi_policy.pdf")
# plt.show()
plt.savefig(f"{PLOT_DIR}/bar_multi_policy.pdf", pad_inches=0)
