#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.
import sqlite3
from datetime import datetime

from plot import Plot, PlotUtils

STEPS = 10
N_NODES = 6
SESSION_ID = datetime.now().strftime("%Y%m%d-%H%M%S")
LEGEND = []

rewards_all = []
for i in range(N_NODES):
    rewards_all.append([])
    LEGEND.append(f"Node{i}")
print(rewards_all)

x_arr = []

for i in range(STEPS + 1):
    db = sqlite3.connect(f"./_log/learning/D_SARSA/20210905-204830-{i}/log.db")
    cur = db.cursor()

    distribution_rt = round(1 / STEPS * i, 2)
    distribution_nrt = round(1 - distribution_rt, 2)

    x_arr.append(distribution_rt)

    rewards = []

    res = cur.execute("select sum(reward) from jobs group by node_uid")
    for j, line in enumerate(res):
        rewards.append(line[0])
        rewards_all[j].append(line[0])

    print(f"{distribution_rt:.2f}/{distribution_nrt:.2f} = {rewards}")

    cur.close()
    db.close()

x_arr_arr = []
for i in range(N_NODES):
    x_arr_arr.append(x_arr)
print(rewards_all)

print(len(rewards_all))
print(len(x_arr_arr))

PlotUtils.use_tex()
Plot.multi_plot(x_arr_arr, rewards_all, x_label="RT Distribution", y_label="Reward", fullpath=f"./_plots/plot_multi_rt_distribution-{SESSION_ID}.pdf", legend=LEGEND)
