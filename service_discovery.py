#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.

from __future__ import annotations

import random
from typing import List

# from node import Node
from log import Log
from node import Node

"""
Implementation of the discovery service for nodes
"""

MODULE = "Discovery"
DEBUG = False


class ServiceDiscovery:
    """Module for allowing nodes cooperation"""

    def __init__(self, nodes):
        self._nodes = nodes  # type: 'List[Node]'
        self._n_nodes = len(nodes)
        self._coefficients = []

        self._nodes_id_dict = {}
        for node in nodes:
            self._nodes_id_dict[node.get_uid()] = node

    def get_random_node(self, current_node_id=None):
        """Get a random node in list except the current one"""
        if current_node_id is not None:
            nodes_list = self._get_all_but_current(current_node_id)
        else:
            nodes_list = self.get_all_nodes()
        if len(nodes_list) == 0:
            return None

        return nodes_list[random.randint(0, len(nodes_list) - 1)]

    def get_n_random_nodes(self, n) -> List[Node]:
        picked_nodes = []
        all_nodes = [node for node in self._nodes]
        if n > self._n_nodes:
            return []

        for i in range(n):
            picked_nodes.append(all_nodes.pop(random.randint(0, len(all_nodes) - 1)))

        return picked_nodes

    def get_node(self, node_id):
        """Return a node by its id"""
        if node_id not in self._nodes_id_dict.keys():
            return None
        return self._nodes_id_dict[node_id]

    def get_node_by_index(self, i):
        return self._nodes[i]

    def get_least_loaded_node(self):
        """Retrieve the node with the least loaded queue"""
        nodes_list = self.get_all_nodes()  # type: List[Node]
        min_value = nodes_list[0].get_load()
        min_i = 0
        for i in range(1, len(nodes_list)):
            if nodes_list[i].get_load() < min_value:
                min_value = nodes_list[i].get_load()
                min_i = i
        return nodes_list[min_i]

    def get_all_nodes(self):
        return self._nodes

    def are_nodes_idle(self) -> bool:
        """Check if all nodes have clear queues and nothing running"""
        for node in self._nodes:
            if not node.is_idle():
                return False

        return True

    #
    # Internals
    #

    def _get_all_but_current(self, current_node_id):
        """Prepare a list of all nodes but the current"""
        nodes_list = []
        for node in self._nodes:
            if node.get_uid() == current_node_id:
                continue
            nodes_list.append(node)
        return nodes_list
