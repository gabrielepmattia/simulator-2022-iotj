#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.
#
#   All rights reserved.

from __future__ import annotations

import simpy

from job import Job
from log import Log
from node import Node
from service_data_storage import ServiceDataStorage
from service_discovery import ServiceDiscovery

"""
Run the simulation of deadline scheduling
"""

MODULE = "Main"


# SIMULATION_TIME = 8000
# SIMULATION_TOTAL_TIME = SIMULATION_TIME + 50
# NODES = 6  # number of nodes

# SESSION_ID = datetime.now().strftime("%Y%m%d-%H%M%S")
# LEARNING_TYPE = Node.LearningType.D_SARSA
# NO_LEARNING_POLICY = Node.NoLearningPolicy.PWR_2_THRESHOLD


class Simulation:

    @staticmethod
    def _simulate(env, simulation_time, n_nodes, session_id, learning_type, no_learning_policy, rt_percentage):
        nodes = []
        # create nodes

        for i in range(n_nodes):
            nodes.append(Node(env,
                              i,
                              session_id,
                              simulation_time=simulation_time,
                              skip_plots=True,
                              # rates
                              rate_mu=None,
                              # traffic model
                              # rate_l_model_path="./traffic/namex/namex-traffic-daily-20210420.csv",
                              rate_l_model_path=f"./traffic/fixed/fixed_{i}.csv",
                              # rate_l_model_path=f"./traffic/city/data/traffic_node_{i}.csv",
                              # rate_l_model_path="./traffic/fictious/fictious_1.csv",
                              rate_l_model_path_shift=0,  # i * 1200,  # 0,
                              rate_l_model_path_cycles=2,
                              rate_l_model_path_parse_x_max=None,
                              rate_l_model_path_steady=False,
                              rate_l_model_path_steady_for=2000,
                              rate_l_model_path_steady_every=2000,
                              # job info
                              job_types=2,
                              job_duration_type=Job.DurationType.GAUSSIAN,
                              job_payload_sizes_mbytes=(0.1, 0.1),
                              job_duration_std_devs=(0.0004, 0.0004),
                              job_percentages=(rt_percentage, 1 - rt_percentage),
                              job_deadlines=(0.016, 0.040),
                              job_durations=(0.008, 0.020),
                              # node info
                              max_jobs_in_queue=5,
                              distribution_arrivals=Node.DistributionArrivals.POISSON,
                              delay_probing=0.003,
                              # learning
                              state_type=Node.StateType.JOB_TYPE,
                              learning_type=learning_type,
                              no_learning_policy=no_learning_policy,
                              actions_space=Node.ActionsSpace.LOCAL_AND_PROBE,
                              pwr2_binary_policy="001111",
                              # threshold=6,
                              # use_model_from_session_name="20210412-125752",
                              # distributions
                              distribution_network_probing=Node.DistributionNetworkProbing.GAUSSIAN,
                              distribution_network_probing_sigma=0.0001,
                              distribution_network_forwarding=Node.DistributionNetworkForwarding.GAUSSIAN,
                              distribution_network_forwarding_sigma=0.00002,
                              episode_length=20,
                              eps=0.9,
                              eps_dynamic=True,
                              eps_min=0.1))

        # add them discovery service
        discovery = ServiceDiscovery(nodes)
        data_storage = ServiceDataStorage(nodes, session_id, learning_type, no_learning_policy)

        # init nodes services, and data
        for node in nodes:
            node.set_service_discovery(discovery)
            node.set_service_data_storage(data_storage)
        for node in nodes:
            node.init()

        Log.minfo(MODULE, "Started simulation")
        env.run(until=simulation_time + 50)
        Log.minfo(MODULE, "Simulation ended")

        data_storage.done_simulation()

    @staticmethod
    def simulate(simulation_time, n_nodes, session_id, learning_type, no_learning_policy, rt_percentage):
        env = simpy.Environment()
        Simulation._simulate(env, simulation_time, n_nodes, session_id, learning_type, no_learning_policy, rt_percentage)
