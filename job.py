#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.

from __future__ import annotations

from enum import Enum
from functools import reduce
from typing import List

import simpy


class Job:
    MODULE = "Job"

    class TransmissionAction(Enum):
        CLIENT_TO_NODE = 0
        NODE_TO_NODE = 1
        NODE_TO_CLIENT = 2

    class DurationType(Enum):
        FIXED = 0
        GAUSSIAN = 1
        EXPONENTIAL = 2

    def __init__(self, env: simpy.Environment, uid=0, node_uid=0, job_type=0, end_clb=None, duration=1.0, duration_std_dev=0.0013, deadline=1.5, eps=0.0, payload_size_mbytes=0.1):
        """Create a new job"""
        self._uid = uid
        """Job identifier"""
        self._node_uid = node_uid
        """Job node generator identifier"""
        self._env = env
        """Simpy environment"""
        self._deadline = deadline  # s
        """Deadline of the job in seconds"""
        """If job is realtime"""
        self._job_duration = duration  # s
        """Average duration of a job"""
        self._job_duration_std_dev = duration_std_dev  # s
        """Standard deviation of a job"""
        self._payload_size = payload_size_mbytes

        self._transmission_next_action = Job.TransmissionAction.CLIENT_TO_NODE
        self._transmission_actions_list = [Job.TransmissionAction.CLIENT_TO_NODE]

        # realtime check
        self._job_type = job_type

        # metrics
        self._metric_execution_time = 0.0  # s
        self._metric_queue_time = 0.0  # s

        # time snapshots
        self._time_generated = env.now
        self._time_queued = 0.0
        self._time_executed = 0.0
        self._time_done = 0.0
        self._time_forwarded = 0.0
        self._time_probing = 0.0
        self._time_rejected = 0.0
        self._time_dispatched = 0.0
        """Time at which"""

        self._forwarded_to_node_id = -1

        # self._current_probing_time = 0.0  # s
        self._dispatched = False
        self._forwarded = False
        self._rejected = False
        self._executed = False
        self._done = False
        """If job path has ended"""

        # data to be logged
        self._state_snapshot = []  # type: List[int]
        """The state of all nodes before dispatching"""
        self._action = 0  # type: int
        """The node to which the job has been dispatched"""
        self._reward = 0.0  # type float
        """The reward for dispatching the job"""
        # callbacks
        self._end_clb = end_clb
        self._episode = 0
        """Episode to which the job belongs"""
        self._last = False
        """If it is the last job of the episode"""
        self._eps = eps
        """The eps value for the job"""

    def __str__(self):
        return f"Job#{self._node_uid}#{self._uid}"

    #
    # Actions
    #

    def a_dispatched(self):
        self._time_dispatched = self._env.now
        self._dispatched = True

    def a_in_queue(self):
        """Add time in queue"""
        self._time_queued = self._env.now

    def a_probing(self):
        """Mark when probing is started"""
        self._time_probing = self._env.now

    def a_executed(self, job_duration: float):
        self._metric_execution_time = job_duration
        self._executed = True

        self._time_executed = self._env.now

    def a_forwarded(self, to_node_uid):
        """Execute when a job is forwarded to another node"""
        self._time_forwarded = self._env.now
        self._forwarded_to_node_id = to_node_uid

    def a_rejected(self):
        self._rejected = True
        self._time_rejected = self._env.now

    def a_done(self):
        """Call this function when job path end, i.e. job output reached the client"""
        # safety checks
        if not self._executed and not self._rejected:
            raise RuntimeError(f"You declared done {self} without being executed or rejected, dispatched={self._dispatched} forwarded={self._forwarded}")
        if (self._rejected or (self._executed and self._forwarded)) and not (len(self._transmission_actions_list) == 2 or len(self._transmission_actions_list) == 4):
            raise RuntimeError(f"Job rejected with bad action list: {self}, executed={self._executed}, forward={self._forwarded}, {self._transmission_actions_list}")

        self._done = True
        self._time_done = self._env.now

        if self._end_clb is not None:
            self._end_clb(self)

    #
    # Exported
    #

    def get_generated_at(self):
        return self._time_generated

    def get_executed_by(self):
        return self._action

    def get_transmission_next_action(self) -> TransmissionAction:
        return self._transmission_next_action

    def get_transmission_actions_list(self) -> List[TransmissionAction]:
        return self._transmission_actions_list

    def is_rejected(self):
        return self._rejected

    def is_forwarded(self):
        return self._forwarded

    def is_dispatched(self):
        return self._dispatched

    def is_over_deadline(self):
        return self.get_total_time() > self._deadline

    def get_type(self) -> int:
        return self._job_type

    def is_executed(self):
        """Get if job has been executed. Obviously, job rejected are not executed"""
        return self._executed

    def is_done(self):
        """Get if job has been executed. Obviously, job rejected are not executed"""
        return self._done

    def get_total_time(self) -> float:
        """Get the total elapsed time for executing the job"""
        # if not self._done:
        #    Log.mwarn(self.MODULE, "Requesting total time while job has not done")
        return self._time_done - self._time_generated

    def get_originator_node_uid(self):
        return self._node_uid

    def get_slack_time(self):
        return self._deadline - self.get_total_time()

    def get_forwarded_to(self):
        return self._forwarded_to_node_id

    def get_probing_time(self):
        return self._time_probing - self._time_generated

    def get_dispatched_time(self):
        return self._time_dispatched - self._time_generated

    def get_queue_time(self):
        return self._time_queued - self._time_generated

    def get_duration(self):
        """Get the programmed job duration"""
        return self._job_duration

    def get_deadline(self):
        return self._deadline

    def get_payload_size(self):
        return self._payload_size

    def get_episode(self):
        return self._episode

    def get_eps(self):
        return self._eps

    def get_uid(self):
        return self._uid

    def get_node_uid(self):
        return self._node_uid

    def is_last_of_episode(self):
        return self._last

    def set_transmission_next_action(self, action: Job.TransmissionAction):
        self._transmission_next_action = action
        self._transmission_actions_list.append(action)

    def set_episode(self, episode):
        self._episode = episode

    def set_last_of_episode(self, last):
        self._last = last

    def set_eps(self, eps):
        self._eps = eps

    #
    # Time generators
    #

    def get_job_duration(self) -> float:
        return self._job_duration

    def get_job_duration_std_dev(self) -> float:
        return self._job_duration_std_dev

    #
    # DNN Data
    #

    def save_state_snapshot(self, snapshot: List[int]):
        self._state_snapshot = snapshot

    def save_action(self, action):
        self._action = action

    def get_state_snapshot(self) -> List[int]:
        return self._state_snapshot

    def get_state_snapshot_str(self) -> List[int]:
        return reduce(lambda x, y: str(x) + str(y), self._state_snapshot)

    def get_action(self) -> int:
        return self._action
