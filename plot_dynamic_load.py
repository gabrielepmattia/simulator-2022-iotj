#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.

import math
import sqlite3

import matplotlib.pyplot as plt

from log import Log
from plot import PlotUtils
from traffic_model import TrafficModel
from utils import Utils

MODULE = "PlotDynamicLoad"

N_NODES = 12

DB_FILE_FOLDER = "20220420-100235"
DB_FILE = f"./_log/learning/D_SARSA/{DB_FILE_FOLDER}/log.db"
# TRAFFIC_MODEL_FILE = "./traffic/namex/namex-traffic-daily-20210420.csv"
TRAFFIC_MODEL_FILE = lambda node: f"./traffic/city/data/traffic_node_{node}.csv"
TRAFFIC_MODEL_CYCLES = 3
TRAFFIC_MODEL_SHIFT = 0

db = sqlite3.connect(DB_FILE)
cur = db.cursor()

simulation_time = 0

res = cur.execute("select max(generated_at) from jobs")
for line in res:
    simulation_time = math.ceil(line[0])

# t_models = []
# for i in range(N_NODES):
#    t_models.append(TrafficModel(raw_path=TRAFFIC_MODEL_FILE(i), cycles=TRAFFIC_MODEL_CYCLES, shift=TRAFFIC_MODEL_SHIFT, max_x=simulation_time))

Log.mdebug(MODULE, f"simulation_time={simulation_time}")

# data
x = []
y_reward = []
x_traffics = []
y_traffics = []

'''
res = cur.execute(f"select cast(generated_at as integer), avg(reward) from jobs where node_uid = 0 group by cast(generated_at as integer)")
avg_reward = 0.0
for line in res:
    t = line[0]
    avg_reward = line[1]

    x.append(t)
    y_reward.append(avg_reward)
    y_traffic.append(t_model.get_traffic_at(t))
'''
#
# retrieve traffic data
#

for i in range(N_NODES):
    y_traffics.append([])
    x_traffics.append([])
    for j in range(simulation_time):
        x_traffics[i].append(j)
        y_traffics[i].append(0.0) # t_models[i].get_traffic_at(j))

#
# retrieve reward data
#

x_rewards = []
y_rewards = []

average_every_secs = 30

for node_i in range(N_NODES):
    x_rewards.append([])
    y_rewards.append([])

    res = cur.execute(f"select cast(generated_at as integer), avg(reward) from jobs where node_uid = {node_i} group by cast(generated_at as integer)")
    sum_reward = 0.0
    added = 0
    for line in res:
        t = line[0]
        reward = line[1]

        sum_reward += reward
        added += 1

        if t % average_every_secs == 0 and t > 0:
            # print(f"t={t}, added={added}, avg={sum_reward / added}")
            x_rewards[node_i].append(t)
            y_rewards[node_i].append(sum_reward / added)
            added = 0
            sum_reward = 0.0

# no learning reward
x_n_rewards = []
y_n_rewards = []

# db2 = sqlite3.connect(DB_FILE) #,"./_log/no-learning/PWR_2_THRESHOLD/20210611-154336/log.db")
db2 = sqlite3.connect("_log/no-learning/PWR_2_THRESHOLD/20220420-095613/log.db")
cur2 = db2.cursor()

for node_i in range(N_NODES):
    x_n_rewards.append([])
    y_n_rewards.append([])

    res2 = cur2.execute(f"select cast(generated_at as integer), avg(reward) from jobs where node_uid = {node_i} group by cast(generated_at as integer)")
    sum_reward = 0.0
    added = 0
    for line in res2:
        t = line[0]
        reward = line[1]

        sum_reward += reward
        added += 1

        if t % average_every_secs == 0 and t > 0:
            # print(f"t={t}, added={added}, avg={avg / added}")
            x_n_rewards[node_i].append(t)
            y_n_rewards[node_i].append(sum_reward / added)
            added = 0
            sum_reward = 0

PlotUtils.use_tex()

cmap_def = plt.get_cmap("tab10")
fig, axis = plt.subplots(nrows=N_NODES, ncols=1)
for node_i in range(N_NODES):
    # axis[node_i].set_title(rf"Node \#{node_i}")
    axis[node_i].plot(x_rewards[node_i], y_rewards[node_i], linewidth='1.2', color=cmap_def(0))
    axis[node_i].plot(x_n_rewards[node_i], y_n_rewards[node_i], linewidth='1.2', color=cmap_def(1))
    axis[node_i].set_ylabel("$\iota$")
    axis[node_i].legend(["Sarsa", "Pwr2 (T=2)"], fontsize=6, loc='lower left')
    axis[node_i].margins(0)
    axis[node_i].tick_params(axis='y', labelsize=8)
    axis[node_i].set_ylim([0, 1])
    axis[node_i].grid(color='#cacaca', linestyle='--', linewidth=0.5)
    axis[node_i].annotate(f'Node \#{node_i}', xy=(1.01, .2), xycoords="axes fraction", rotation="-90")
    if node_i != N_NODES - 1:
        axis[node_i].set_xticklabels([])
    else:
        axis[node_i].set_xlabel("Time (s)")

"""
for i in range(N_NODES):
    axis[N_NODES].plot(x_traffics[i], y_traffics[i], linewidth='1', color=cmap_def(i))
axis[N_NODES].set_ylim([0, 1])
axis[N_NODES].set_xlabel("Time")
axis[N_NODES].set_ylabel(r"$\rho$")
axis[N_NODES].margins(0)
axis[N_NODES].grid(color='#cacaca', linestyle='--', linewidth=0.5)
axis[N_NODES].legend([f"To Node \#{i}" for i in range(N_NODES)], fontsize=3, loc='lower right')
"""

# size and padding
fig.tight_layout(h_pad=0)
fig.set_figwidth(6.4)  # 6.4
fig.set_figheight(5)  # 4.8
fig.subplots_adjust(wspace=0, hspace=0.2)

# plt.show()
plt.savefig(f"_plots/dynamic_plot-node_{DB_FILE_FOLDER}_{Utils.current_time_string()}.pdf")
