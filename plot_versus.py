#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.

import os
import sqlite3
from functools import reduce

from log import Log
from plot import Plot, PlotUtils

MODULE = "PlotVersus"

PLOTS_DIR = "_plots"
DB_DIRS = [
    "./_log/learning/D_SARSA/20210420-125838/log.db",
    # "./_log/no-learning/RANDOM/20210412-142858/log.db",
    "./_log/no-learning/PWR_2_THRESHOLD/20210412-174913/log.db",
    # "./_log/no-learning/PWR_2_THRESHOLD/20210412-142928/log.db",
    "./_log/no-learning/PWR_2_THRESHOLD/20210412-174918/log.db",
]
LEGEND = [
    "Sarsa",
    # "Random",
    "Pwr2 (T=0)",
    # "Pwr2 (T=2)",
    "No Cooperation",
]

AVERAGE_JOBS_COMPUTATION = 200
AVERAGE_EPISODES_COMPUTATION = 10
JOBS_PER_ROUND = 20

MAX_JOBS_IN_QUEUE = 5

os.makedirs(PLOTS_DIR, exist_ok=True)


def plot_reward_for_rounds():
    Log.minfo(MODULE, "Plotting plot_reward_for_rounds")

    # noinspection SqlNoDataSourceInspection
    def extract_data_from_db(db_path):
        db = sqlite3.connect(db_path)
        cur = db.cursor()

        res = cur.execute(
            """WITH RECURSIVE cnt(x) AS (VALUES(1) UNION ALL SELECT x+1 FROM cnt WHERE x< (select max(episode) from jobs))
                SELECT x as episode, avg_reward FROM cnt LEFT OUTER JOIN (
                (SELECT episode, AVG(reward) as avg_reward FROM (SELECT episode, node_uid, SUM(reward) as reward FROM jobs GROUP BY episode, node_uid) GROUP BY episode)) as new
                ON cnt.x = new.episode""")

        x_arr = []
        y_arr = []
        avg = 0.0
        j = 0
        for i, line in enumerate(res):
            avg += line[1]

            if i % AVERAGE_JOBS_COMPUTATION == 0 and i != 0:
                x_arr.append(j)
                y_arr.append(avg / AVERAGE_JOBS_COMPUTATION)

                avg = 0
                j += 1

        cur.close()
        db.close()
        return x_arr, y_arr

    x_arrs = []
    y_arrs = []
    for path in DB_DIRS:
        Log.minfo(MODULE, f"Plotting {path}")

        x_arr, y_arr = extract_data_from_db(path)
        x_arrs.append(x_arr)
        y_arrs.append(y_arr)

    PlotUtils.use_tex()
    Plot.multi_plot(x_arrs, y_arrs, x_label=fr"Jobs $\times $ {AVERAGE_JOBS_COMPUTATION * JOBS_PER_ROUND}",
                    y_label="Reward", fullpath=f"{PLOTS_DIR}/rewards_versus.pdf",
                    legend=LEGEND,
                    show_markers=False, linewidth=1.0)


def plot_total_time_for_rounds():
    # noinspection SqlNoDataSourceInspection
    Log.minfo(MODULE, "Plotting plot_total_time_for_rounds")

    def extract_data_from_db(db_path):
        db = sqlite3.connect(db_path)
        cur = db.cursor()

        res = cur.execute(
            """SELECT episode, AVG(time_total) as time_total FROM (SELECT episode, node_uid, AVG(time_total) as time_total FROM jobs WHERE executed = 1 GROUP BY episode, node_uid) GROUP BY episode""")

        x_arr = []
        y_arr = []
        avg = 0.0
        j = 0
        for i, line in enumerate(res):
            avg += line[1]

            if i % AVERAGE_JOBS_COMPUTATION == 0 and i != 0:
                x_arr.append(j)
                y_arr.append(avg / AVERAGE_JOBS_COMPUTATION)

                avg = 0
                j += 1

        cur.close()
        db.close()

        return x_arr, y_arr

    x_arrs = []
    y_arrs = []
    for path in DB_DIRS:
        Log.minfo(MODULE, f"Plotting {path}")

        x_arr, y_arr = extract_data_from_db(path)
        x_arrs.append(x_arr)
        y_arrs.append(y_arr)

    PlotUtils.use_tex()
    Plot.multi_plot(x_arrs, y_arrs, x_label=fr"Jobs $\times $ {AVERAGE_JOBS_COMPUTATION * JOBS_PER_ROUND}",
                    y_label="Total Time (s)", fullpath=f"{PLOTS_DIR}/time_total_versus.pdf",
                    legend=LEGEND, show_markers=False, linewidth=1.0)


def plot_rejected_for_rounds():
    # noinspection SqlNoDataSourceInspection
    Log.minfo(MODULE, "Plotting plot_rejected_for_rounds")

    def extract_data_from_db(db_path):
        db = sqlite3.connect(db_path)
        cur = db.cursor()

        res = cur.execute(
            """WITH RECURSIVE cnt(x) AS (VALUES(1) UNION ALL SELECT x+1 FROM cnt WHERE x< (select max(episode) from jobs))
                SELECT x as episode, avg_rejected FROM cnt LEFT OUTER JOIN (
                (SELECT episode, AVG(rejected) as avg_rejected FROM (SELECT episode, node_uid, COUNT(rejected) as rejected FROM jobs WHERE rejected = 1 GROUP BY episode, node_uid) GROUP BY episode)) as new
                ON cnt.x = new.episode""")

        x_arr = []
        y_arr = []
        summation = 0.0
        j = 0
        i = 0
        for line in res:
            summation += line[1] if line[1] is not None else 0.0

            if i % AVERAGE_JOBS_COMPUTATION == 0 and i != 0:
                x_arr.append(j)
                y_arr.append(summation / AVERAGE_JOBS_COMPUTATION)

                summation = 0
                j += 1

            i += 1

        Log.mdebug(MODULE, f"Total lines {i}, total bins {j}")

        cur.close()
        db.close()

        return x_arr, y_arr

    x_arrs = []
    y_arrs = []
    for path in DB_DIRS:
        Log.minfo(MODULE, f"Plotting {path}")

        x_arr, y_arr = extract_data_from_db(path)
        x_arrs.append(x_arr)
        y_arrs.append(y_arr)

    PlotUtils.use_tex()
    Plot.multi_plot(x_arrs, y_arrs, x_label=fr"Jobs $\times $ {AVERAGE_JOBS_COMPUTATION * JOBS_PER_ROUND}",
                    y_label="Rejected Jobs (s)",
                    fullpath=f"{PLOTS_DIR}/rejected_versus.pdf",
                    legend=LEGEND, show_markers=False, linewidth=1.0)


def plot_executed_for_rounds():
    # noinspection SqlNoDataSourceInspection
    Log.minfo(MODULE, "Plotting plot_executed_for_rounds")

    def extract_data_from_db(db_path):
        db = sqlite3.connect(db_path)
        cur = db.cursor()

        res = cur.execute(
            """WITH RECURSIVE cnt(x) AS (VALUES(1) UNION ALL SELECT x+1 FROM cnt WHERE x< (select max(episode) from jobs))
                SELECT x as episode, avg_executed FROM cnt LEFT OUTER JOIN (
                (SELECT episode, AVG(executed) as avg_executed FROM (SELECT episode, node_uid, COUNT(executed) as executed FROM jobs WHERE executed = 1 GROUP BY episode, node_uid) GROUP BY episode)) as new
                ON cnt.x = new.episode""")

        x_arr = []
        y_arr = []
        avg = 0.0
        j = 0
        for i, line in enumerate(res):
            avg += line[1] if line[1] is not None else 0.0

            if i % AVERAGE_JOBS_COMPUTATION == 0 and i != 0:
                x_arr.append(j)
                y_arr.append(avg / AVERAGE_JOBS_COMPUTATION)

                avg = 0
                j += 1

        cur.close()
        db.close()

        return x_arr, y_arr

    x_arrs = []
    y_arrs = []
    for path in DB_DIRS:
        Log.minfo(MODULE, f"Plotting {path}")

        x_arr, y_arr = extract_data_from_db(path)
        x_arrs.append(x_arr)
        y_arrs.append(y_arr)

    PlotUtils.use_tex()
    Plot.multi_plot(x_arrs, y_arrs, x_label=fr"Jobs $\times $ {AVERAGE_JOBS_COMPUTATION * JOBS_PER_ROUND}",
                    y_label="Executed Jobs (s)",
                    fullpath=f"{PLOTS_DIR}/executed_versus.pdf",
                    legend=LEGEND, show_markers=False, linewidth=1.0)


def plot_overdeadline_for_rounds():
    Log.minfo(MODULE, "Plotting plot_overdeadline_for_rounds")

    # noinspection SqlNoDataSourceInspection
    def extract_data_from_db(db_path):
        db = sqlite3.connect(db_path)
        cur = db.cursor()

        res = cur.execute(
            """WITH RECURSIVE cnt(x) AS (VALUES(1) UNION ALL SELECT x+1 FROM cnt WHERE x< (select max(episode) from jobs))
                SELECT x as episode, avg_over_deadline FROM cnt LEFT OUTER JOIN (
                (SELECT episode, AVG(over_deadline) as avg_over_deadline FROM (SELECT episode, node_uid, COUNT(over_deadline) as over_deadline FROM jobs WHERE executed = 1 and over_deadline = 1 GROUP BY episode, node_uid) GROUP BY episode)) as new
                ON cnt.x = new.episode""")

        x_arr = []
        y_arr = []
        avg = 0.0
        j = 0
        for i, line in enumerate(res):
            avg += line[1] if line[1] is not None else 0.0

            if i % AVERAGE_JOBS_COMPUTATION == 0 and i != 0:
                x_arr.append(j)
                y_arr.append(avg / AVERAGE_JOBS_COMPUTATION)

                avg = 0
                j += 1

        cur.close()
        db.close()

        return x_arr, y_arr

    x_arrs = []
    y_arrs = []
    for path in DB_DIRS:
        Log.minfo(MODULE, f"Plotting {path}")

        x_arr, y_arr = extract_data_from_db(path)
        x_arrs.append(x_arr)
        y_arrs.append(y_arr)

    PlotUtils.use_tex()
    Plot.multi_plot(x_arrs, y_arrs, x_label=fr"Jobs $\times $ {AVERAGE_JOBS_COMPUTATION * JOBS_PER_ROUND}",
                    y_label="Over-deadline Jobs (s)",
                    fullpath=f"{PLOTS_DIR}/overdeadline_versus.pdf",
                    legend=LEGEND, show_markers=False, linewidth=1.0)


def plot_loss_for_rounds():
    # noinspection SqlNoDataSourceInspection
    Log.minfo(MODULE, "Plotting plot_loss_for_rounds")

    def extract_data_from_db(db_path):
        db = sqlite3.connect(db_path)
        cur = db.cursor()

        # noinspection SqlNoDataSourceInspection
        res = cur.execute("""select episode, avg(loss) from episodes group by episode""")

        x_arr = []
        y_arr = []
        avg = 0.0
        j = 0
        for i, line in enumerate(res):
            avg += abs(line[1])

            if i % AVERAGE_JOBS_COMPUTATION == 0 and i != 0:
                x_arr.append(j)
                y_arr.append(avg / AVERAGE_EPISODES_COMPUTATION)

                avg = 0
                j += 1

        cur.close()
        db.close()

        return x_arr, y_arr

    x_arrs = []
    y_arrs = []
    for path in DB_DIRS[0:1]:
        Log.minfo(MODULE, f"Plotting {path}")

        x_arr, y_arr = extract_data_from_db(path)
        x_arrs.append(x_arr)
        y_arrs.append(y_arr)

    PlotUtils.use_tex()
    Plot.multi_plot(x_arrs, y_arrs, x_label=fr"Jobs $\times $ {AVERAGE_EPISODES_COMPUTATION * JOBS_PER_ROUND}",
                    y_label="Loss", fullpath=f"{PLOTS_DIR}/loss_versus.pdf",
                    legend=LEGEND, show_markers=False, linewidth=1.0)


# noinspection PyTypeChecker, SqlNoDataSourceInspection
def plot_job_distribution():
    Log.minfo(MODULE, "Plotting plot_job_distribution")

    db = sqlite3.connect(DB_DIRS[0])
    cur = db.cursor()

    states = []
    states_values = []
    for i in range(MAX_JOBS_IN_QUEUE + 1):
        for j in range(2):  # non-realtime / realtime
            state_string = reduce(lambda x, y: str(x) + str(y), [i, j])
            states.append(state_string)

            # noinspection SqlNoDataSourceInspection
            res = cur.execute(f'''select count(*) from jobs where state_snapshot = "{state_string}"''')
            for line in res:
                states_values.append(int(line[0]))

    PlotUtils.use_tex()
    Plot.plot_bar_chart(states, states_values, x_label=fr"States (Load | Realtime)", y_label="Number of Jobs",
                        fullpath=f"{PLOTS_DIR}/states_job_distribution.pdf")

    cur.close()
    db.close()

    return states, states_values


# noinspection PyTypeChecker, SqlNoDataSourceInspection
def plot_policy_heatmap():
    Log.minfo(MODULE, "Plotting plot_policy_heatmap")

    states_states, values = plot_job_distribution()
    max_v = max(values)
    d = {}
    for i in range(len(states_states)):
        d[states_states[i]] = values[i] / max_v

    print(plot_policy_heatmap)
    print(states_states)
    print(values)

    db = sqlite3.connect(DB_DIRS[0])
    cur = db.cursor()

    mat = []
    y_ticks = ["Non-Probe", "Probe"]
    x_ticks = []
    for i in range(2):  # non-probe / probe
        mat.append([])
        for j in range(MAX_JOBS_IN_QUEUE + 1):
            action = i
            state_string = reduce(lambda x, y: str(x) + str(y), [j, 1])
            if i == 0:
                x_ticks.append(state_string)
            print(f"a={action}, s={state_string}")

            res = cur.execute(
                f'''select state, action, avg(value) from q_values where action = {action} and state = "{state_string}" group by state, action''')
            for line in res:
                mat[i].append(line[2])
    print(mat)

    for j in range(len(mat[0])):
        max_col = mat[0][j]
        for i in range(len(mat)):
            if mat[i][j] > max_col:
                max_col = mat[i][j]
        for i in range(len(mat)):
            if mat[i][j] == max_col:
                mat[i][j] = 1.0 * d[f"{j}1"]
            else:
                mat[i][j] = -1.0 * (1 - d[f"{j}1"])

    print(mat)

    cur.close()
    db.close()

    PlotUtils.use_tex()
    Plot.plot_heatmap(mat, x_ticks=x_ticks, y_ticks=y_ticks, fullpath=f"{PLOTS_DIR}/heat_policy.pdf",
                      cbarlabel="Decision Confidence", x_label="State", y_label="Action")


plot_policy_heatmap()
plot_job_distribution()
plot_reward_for_rounds()
plot_total_time_for_rounds()
plot_rejected_for_rounds()
plot_executed_for_rounds()
plot_overdeadline_for_rounds()
plot_loss_for_rounds()
