#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.

from __future__ import annotations

import os
import pickle
import random
import traceback
from enum import Enum
from typing import List

import numpy as np
import simpy

from function_approximation import DSPSarsaTiling
from job import Job
from log import Log
from traffic_model import TrafficModel

"""
Implementation of a node in a fog environment
"""

DEBUG = False
MODULE = "Node"


class DiscoveryType:
    NO_COOPERATION = 0
    BEST_DEADLINE_ORACLE = 1
    BEST_DEADLINE_PWR_1 = 2
    BEST_DEADLINE_PWR_2 = 3
    PWR_1 = 4
    LEAST_LOADED = 5


# noinspection DuplicatedCode
class Node:
    MODULE = "Node"

    BASE_DIR_LOG = "_log"
    BASE_DIR_PLOT = "_plot"
    BASE_DIR_DNN = "_dnn"
    BASE_DIR_MODELS = "_models"
    BASE_DIR_TB = "_scalars"

    LOG_KEY_EPISODE = "episode"
    LOG_KEY_EPS = "eps"
    LOG_KEY_SCORE = "score"
    LOG_KEY_TOTAL_JOBS = "total_jobs"
    LOG_KEY_LOSS = "loss"
    LOG_KEY_MSE = "mse"
    LOG_KEY_MAE = "mae"

    LOG_KEYS = [LOG_KEY_EPISODE, LOG_KEY_EPS, LOG_KEY_SCORE, LOG_KEY_TOTAL_JOBS, LOG_KEY_LOSS, LOG_KEY_MSE, LOG_KEY_MAE]

    LOG_POS_EPISODE = 0
    LOG_POS_EPS = 1
    LOG_POS_SCORE = 2
    LOG_POS_TOTAL_JOBS = 3
    LOG_POS_LOSS = 4
    LOG_POS_MSE = 5
    LOG_POS_MAE = 6

    LOG_KEYS_POS = [LOG_POS_EPISODE, LOG_POS_EPS, LOG_POS_SCORE, LOG_POS_TOTAL_JOBS, LOG_POS_LOSS, LOG_POS_MSE,
                    LOG_POS_MAE]

    class ActionsType(Enum):
        """Types of actions that the agent can perform"""
        ALL = 0  # all actions, i.e. schedule to all nodes
        AVAILABLE = 1  # only actions that are valid according to _get_actions
        AVAILABLE_AWARE = 2  # only actions that are valid according to _get_actions and the deadline

    class ActionsSpace(Enum):
        LOCAL_AND_PROBE = 0  # possible actions are only execute local and random probe
        FORWARD_TO_AND_PROBE = 1  # possible actions are execute local, probe random, forward to node X
        REJECT_LOCAL_AND_FORWARD_TO = 2  # possible actions are reject, execute local and forward to node X

    class LearningType(Enum):
        NO_LEARNING = 0
        Q_DNN = 1
        Q_TABLE = 2
        D_SARSA = 3

    class NoLearningPolicy(Enum):
        LEAST_LOADED = 0
        LEAST_LOADED_AWARE = 1
        RANDOM = 2
        PWR_2 = 3
        PWR_2_THRESHOLD = 4

    class DistributionArrivals(Enum):
        POISSON = 0
        DETERMINISTIC = 1

    class DistributionNetworkProbing(Enum):
        DETERMINISTIC = 0
        GAUSSIAN = 1

    class DistributionNetworkForwarding(Enum):
        DETERMINISTIC = 0
        GAUSSIAN = 1

    class StateType(Enum):
        ONLY_NUMBER = 0  # state is only the number of queued jobs
        JOB_TYPE = 1  # state is the number of queued job for each type

    ACTION_REJECT = 0  # directly execute the job locally
    ACTION_EXECUTE_LOCALLY = 1  # directly execute the job locally
    ACTION_PROBE = 2  # perform a probe and executes the job remotely if its load is lesser than ours

    # noinspection PyUnresolvedReferences
    def __init__(self, env, uid, session_uid,
                 simulation_time=50000,
                 discovery=None,
                 data_storage=None,
                 # machine
                 max_jobs_in_queue=10,
                 max_jobs_running=4,
                 machine_speed=1.0,
                 rate_mu=None,
                 # jobs
                 job_types=2,
                 job_duration_type=Job.DurationType.GAUSSIAN,
                 job_payload_sizes_mbytes=(0.1, 0.1),
                 job_duration_std_devs=(0.0013, 0.0013),
                 job_percentages=(.5, .5),
                 job_deadlines=(0.016, 0.040),
                 job_durations=(0.008, 0.020),
                 # network
                 net_speed_nodes_mbits=1000,
                 net_speed_nodes_client_mbits=1000,
                 delay_probing=0.004,
                 # traffic
                 rate_l=1.0,
                 rate_l_array=None,
                 rate_l_model_path=None,
                 rate_l_model_path_cycles=1,
                 rate_l_model_path_shift=0,
                 rate_l_model_path_parse_x_max=None,
                 rate_l_model_path_steady=False,
                 rate_l_model_path_steady_for=2000,
                 rate_l_model_path_steady_every=2000,
                 # distributions
                 distribution_arrivals=DistributionArrivals.POISSON,
                 distribution_network_probing=DistributionNetworkProbing.GAUSSIAN,
                 distribution_network_probing_sigma=0.0002,
                 distribution_network_forwarding=DistributionNetworkForwarding.GAUSSIAN,
                 distribution_network_forwarding_sigma=0.00002,
                 # actions
                 actions_space=ActionsSpace.LOCAL_AND_PROBE,
                 actions_type=ActionsType.ALL,
                 # learning
                 state_type=StateType.JOB_TYPE,
                 episode_length=None,
                 batch_size=0,
                 use_model_from_session_name=None,
                 eps=0.1, eps_min=0.05, eps_decay=0.999,
                 learning_type=LearningType.NO_LEARNING,
                 no_learning_policy=NoLearningPolicy.RANDOM,
                 eps_dynamic=False,
                 # other parameters
                 skip_plots=False,
                 pwr2_threshold=3,
                 pwr2_binary_policy=None  # express like '11111'
                 ):

        #
        # Fixed variables
        #

        self._env = env  # type: 'simpy.Environment'

        # node parameters
        self._simulation_time = simulation_time
        """Total time of the simulation"""
        self._uid = random.randint(0, 100000) if uid is None else uid
        """Node id"""
        self._session_uid = session_uid
        """Session id"""
        if self._session_uid is None:
            Log.merr(f"{MODULE}#{self._uid}", f"Node#{id}: session_uid cannot be none")
            exit(1)

        # machine
        self._max_jobs_in_queue = max_jobs_in_queue  # = to 'k'
        self._max_jobs_running = max_jobs_running
        """Maximum number of jobs in the queue"""
        self._machine_speed = machine_speed
        """Multiplier of the job duration before applying the distribution function"""
        self._rate_mu = rate_mu
        """mu used for deriving traffic from ro"""

        # traffic
        self._rate_l = rate_l
        self._rate_l_array = rate_l_array
        """Job arrival rate"""
        # traffic model
        self._traffic_model = TrafficModel(
            raw_path=rate_l_model_path,
            max_x=simulation_time,
            cycles=rate_l_model_path_cycles,
            shift=rate_l_model_path_shift,
            parsed_x_limit=rate_l_model_path_parse_x_max,
            steady=rate_l_model_path_steady,
            steady_for=rate_l_model_path_steady_for,
            steady_every=rate_l_model_path_steady_every,
        ) if rate_l_model_path is not None else None

        # network
        self._delay_probing = delay_probing  # s
        self._net_speed_nodes_mbits = net_speed_nodes_mbits
        self._net_speed_nodes_client_mbits = net_speed_nodes_client_mbits

        # jobs
        self._job_duration_type = job_duration_type
        """Duration distribution of jobs executed in the node"""
        self._job_types = job_types
        self._job_durations = job_durations
        self._job_deadlines = job_deadlines
        self._job_percentages = job_percentages
        self._job_duration_std_devs = job_duration_std_devs
        """Percentage of jobs that arrives and have a realtime deadline"""
        if sum(self._job_percentages) != 1.0:
            raise ValueError(f"Job percentages is not summing to 1: {job_percentages}")
        self._job_payload_sizes_mbytes = job_payload_sizes_mbytes
        # update mu
        if self._rate_mu is None:
            self._rate_mu = 0.0
            for i in range(job_types):
                self._rate_mu += (1 / self._job_durations[i]) * self._job_percentages[i]

        # actions
        self._state_type = state_type
        self._actions_type = actions_type
        self._actions_space = actions_space
        """The type of actions among which the agent can choose"""
        # learning
        self._epsilon = eps  # exploration rate
        """Exploration rate"""
        self._epsilon_min = eps_min
        self._epsilon_decay = eps_decay
        self._eps_dynamic = eps_dynamic
        """If eps should be updated automatically"""
        self._session_learning_type = learning_type
        self._session_no_learning_policy = no_learning_policy
        self._use_model_from_session_name = use_model_from_session_name

        # distribution
        self._distribution_network_probing = distribution_network_probing
        self._distribution_network_probing_sigma = distribution_network_probing_sigma
        self._distribution_network_forwarding = distribution_network_forwarding
        self._distribution_network_forwarding_sigma = distribution_network_forwarding_sigma
        self._distribution_arrivals = distribution_arrivals
        """The statistical distribution for arrival jobs"""

        # other
        self._pwr2_threshold = pwr2_threshold
        self._pwr2_binary_policy = pwr2_binary_policy
        self._skip_plots = skip_plots

        #
        # Runtime variables
        #

        # services
        self._service_discovery = discovery  # type: DiscoveryService
        """Discovery service"""
        self._service_data_storage = data_storage  # type: 'DataStorage'
        """Data storage service"""

        # containers
        self._queued_jobs = simpy.resources.container.Container(self._env, capacity=self._max_jobs_in_queue, init=0)
        """Queued jobs container"""
        self._queued_probe_jobs = simpy.resources.container.Container(self._env, init=0)
        """Queued probing jobs container"""
        self._queued_transmission_jobs = simpy.resources.container.Container(self._env, init=0)
        """Queued transmission jobs container"""

        # processes
        self._process_workers_list = []
        for worker_i in range(self._max_jobs_running):
            self._process_workers_list.append(self._env.process(self._process_job_executor(executor_id=worker_i)))
        # self._process_worker = self._env.process(self._process_job_executor())  # type: simpy.Process
        """Process of job processor"""
        self._process_generator = self._env.process(self._process_jobs_generator())  # type: simpy.Process
        """Process of job generator"""
        self._process_probing = self._env.process(self._process_job_probing())  # type: simpy.Process
        """Process that simulates probing requests"""
        self._process_transmission = self._env.process(self._process_job_transmission())  # type: simpy.Process
        """Process that simulates probing requests"""
        self._process_logger = self._env.process(self._process_logger_impl())  # type: simpy.Process
        """Process that simulates probing requests"""
        # self._process_shutdown = self._env.process(self._process_node_shutdown())  # type: simpy.Process
        """Process of job generator shutdown"""
        self._jobs_list = []
        """List of jobs in the queue"""
        self._jobs_probing_list = []
        """List of jobs enqueue for probing"""
        self._jobs_transmission_list = []
        """List of jobs enqueue for transmission"""

        self._currently_executing_jobs_list = []  # type: 'List[Job]'
        """The current job that is executing"""

        # counters and lists
        self._total_jobs = 0
        """Total number of dispatched jobs"""
        self._total_processed_job = 0
        """Total number of jobs memorized"""
        self._last_episode_end_at = 0
        """The number of job at which the last episode end"""
        self._scheduled_jobs = []  # type: List[Job]
        """List of episode jobs"""
        self._current_episode_number = 0
        """Number of total episodes"""
        self._last_logged_q_value_time = -1

        # logging
        self._data_log = {}  # episode, eps, score, total_jobs
        for key in self.LOG_KEYS:
            self._data_log[key] = []

        # to init later
        self._n_nodes = 0  # to be init
        """Total number of nodes"""
        self._episode_length = episode_length  # to be init -- episode_length if episode_length is not None else self._n_nodes * 3
        """Total number of jobs for declaring the end of an episode"""
        self._batch_size = batch_size  # to be init -- batch_size if batch_size is not None else self._episode_length * 3
        """Total number of jobs for creating a batch when replay"""

        self._action_size = 0  # to be init -- self._n_nodes + 1
        """Size of the action space, all nodes + reject job (last action)"""
        self._states_number = 0  # to be init -- pow(self._n_nodes, self._params.k + 1)
        """Size of the state space"""
        self._all_possible_states = None

    #
    # Init
    #

    def init(self):
        """Init the node params after services have been installed"""
        self._n_nodes = len(self._service_discovery.get_all_nodes())

        if self._actions_space is Node.ActionsSpace.LOCAL_AND_PROBE:
            self._action_size = 2  # execute locally or probe random
        elif self._actions_space is Node.ActionsSpace.FORWARD_TO_AND_PROBE:
            self._action_size = 2 + self._n_nodes - 1  # execute locally or probe random
        elif self._actions_space is Node.ActionsSpace.REJECT_LOCAL_AND_FORWARD_TO:
            self._action_size = 2 + self._n_nodes - 1  # execute locally or probe random
        else:
            Log.merr(f"{MODULE}#{self._uid}", "self._actions_space is not valid")
            raise RuntimeError("ActionsSpace not valid")

        self._episode_length = self._episode_length if self._episode_length > 0 else self._n_nodes * 3

        # init learning strategy and parameters
        if self._session_learning_type is not Node.LearningType.NO_LEARNING:
            self._batch_size = self._batch_size if self._batch_size > 0 else self._episode_length * 3
            self._states_number = self._max_jobs_in_queue * 2  # self._action_size  # self._max_jobs_in_queue  the current node state is the state

            Log.minfo(self._module(),
                      f"_init: episode_length={self._episode_length}, self._batch_size={self._batch_size}, "
                      f"self._action_size={self._action_size}, self._states_number={self._states_number}")

            # init learning strategy
            if self._session_learning_type == Node.LearningType.Q_DNN:
                # self._init_q_dnn()
                pass
            elif self._session_learning_type == Node.LearningType.Q_TABLE:
                # self._init_q_table()
                pass
            elif self._session_learning_type == Node.LearningType.D_SARSA:
                self._init_d_sarsa()

        self._all_possible_states = self._get_all_possible_states_str()

        self._init_dirs()
        Log.minfo(f"{MODULE}#{self._uid}", f"Node#{self._uid} completed init for session#{self._session_uid}")

    def _init_dirs(self):
        if self._session_learning_type == Node.LearningType.NO_LEARNING:
            self._DIR_LOG = f"{Node.BASE_DIR_LOG}/no-learning/{self._session_no_learning_policy.name}/{self._session_uid}/Node#{self._uid}"
            self._DIR_PLOT = f"{Node.BASE_DIR_PLOT}/no-learning/{self._session_no_learning_policy.name}/{self._session_uid}/Node#{self._uid}"
        else:
            self._DIR_MODELS = f"{Node.BASE_DIR_MODELS}/{self._session_learning_type}/{self._session_uid}/Node#{self._uid}"
            self._DIR_TB_LOG = f"{Node.BASE_DIR_TB}/{self._session_learning_type}/{self._session_uid}/Node#{self._uid}"
            self._DIR_LOG = f"{Node.BASE_DIR_LOG}/learning/{self._session_learning_type.name}/{self._session_uid}/Node#{self._uid}"
            self._DIR_PLOT = f"{Node.BASE_DIR_PLOT}/learning/{self._session_learning_type.name}/{self._session_uid}/Node#{self._uid}"
            os.makedirs(self._DIR_MODELS, exist_ok=True)
            if self._session_learning_type == Node.LearningType.Q_DNN:
                pass
                # os.makedirs(self._DIR_TB_LOG, exist_ok=True)
                # file_writer = tf.summary.create_file_writer(self._DIR_TB_LOG + "/metrics")
                # file_writer.set_as_default()

        os.makedirs(self._DIR_LOG, exist_ok=True)
        os.makedirs(self._DIR_PLOT, exist_ok=True)

    #
    # Processes
    #

    def _process_job_executor(self, executor_id=0):
        """Process which executes jobs for a node"""
        if DEBUG:
            Log.mdebug(f"{MODULE}#{self._uid}", f"job executor #{executor_id} started")

        currently_executing_job = None
        while True:
            try:
                # wait for a job
                yield self._queued_jobs.get(1)
                currently_executing_job = self._jobs_list.pop(0)

                self._currently_executing_jobs_list.append(currently_executing_job)

                # compute the actual job duration according to parameters
                actual_job_duration = self._job_compute_duration(currently_executing_job)
                if len(self._currently_executing_jobs_list) > 1:
                    actual_job_duration *= (1.0 + 0.1 * len(self._currently_executing_jobs_list))

                # execute it
                yield self._env.timeout(actual_job_duration)

                # update exec time
                currently_executing_job.a_executed(actual_job_duration)

                self._currently_executing_jobs_list.remove(currently_executing_job)

                # read to the transmission queue
                self._job_transmit(currently_executing_job)

                if DEBUG:
                    Log.mdebug(f"{MODULE}#{self._uid}",
                               f"_process_job_executor #{executor_id}: executed job={currently_executing_job}, next_action={currently_executing_job.get_transmission_next_action()}, level={self._queued_jobs.level}")

                currently_executing_job = None

            except Exception as e:
                # if DEBUG:
                if currently_executing_job is not None:
                    Log.merr(f"{MODULE}#{self._uid}",
                             f"Node#{self.get_uid()} job executor #{executor_id} interrupted, currently executing job={currently_executing_job} e={e}")
                else:
                    Log.merr(f"{MODULE}#{self._uid}",
                             f"Node#{self.get_uid()} job executor #{executor_id} interrupted, e={e}")

    def _process_job_probing(self):
        """Process which simulates a probing"""
        if DEBUG:
            Log.mdebug(f"{MODULE}#{self._uid}", f"job executor started")

        def actual_time(delay):
            if self._distribution_network_probing == Node.DistributionNetworkProbing.GAUSSIAN:
                return random.gauss(delay, self._distribution_network_probing_sigma)
            return delay

        while True:
            try:
                # wait for a job
                yield self._queued_probe_jobs.get(1)
                job = self._jobs_probing_list.pop(0)

                # send the probing
                yield self._env.timeout(actual_time(self._delay_probing / 2))

                # pick the state of a random node
                # probe 1 random node
                random_node = self._service_discovery.get_random_node(current_node_id=self.get_uid())  # type: Node
                random_node_load = random_node.get_load()

                # return
                yield self._env.timeout(actual_time(self._delay_probing / 2))

                # check the load
                if random_node_load < job.get_state_snapshot()[0]:
                    self._job_forward_to(job, random_node.get_uid())
                else:
                    # schedule locally
                    job.set_transmission_next_action(Job.TransmissionAction.NODE_TO_CLIENT)
                    self._job_schedule(job)

                if DEBUG:
                    Log.mdebug(f"{MODULE}#{self._uid}",
                               f"{job} probe executed: forwarded_to={job.get_forwarded_to()} now={self._env.now:.4f} queued_jobs={self._queued_probe_jobs.level}")

            except Exception as e:
                # if DEBUG:
                if len(self._currently_executing_jobs_list) > 0:
                    Log.merr(f"{MODULE}#{self._uid}",
                             f"Node#{self.get_uid()} job probe executor interrupted, currently executing jobs={self._currently_executing_jobs_list} e={e}")
                else:
                    Log.merr(f"{MODULE}#{self._uid}", f"Node#{self.get_uid()} job probe executor interrupted, e={e}")

    def _process_job_transmission(self):
        """Process which simulates a job transmission"""
        if DEBUG:
            Log.mdebug(f"{MODULE}#{self._uid}", f"job transmission process started")

        def actual_time(delay):
            if self._distribution_network_forwarding == Node.DistributionNetworkForwarding.GAUSSIAN:
                return random.gauss(delay, self._distribution_network_forwarding_sigma)
            return delay

        while True:
            try:
                # wait for a job
                yield self._queued_transmission_jobs.get(1)
                job = self._jobs_transmission_list.pop(0)
                next_action = job.get_transmission_next_action()

                if DEBUG:
                    Log.mdebug(f"{MODULE}#{self._uid}",
                               f"_process_job_transmission: start transmitting job={job}, action={job.get_transmission_next_action()}")

                if next_action is Job.TransmissionAction.CLIENT_TO_NODE:
                    # job is sent from the client to the node
                    to_wait = job.get_payload_size() * 8 / self._net_speed_nodes_client_mbits
                    yield self._env.timeout(actual_time(to_wait))
                    # execute the first decision when arrived
                    self._job_first_dispatching(job)

                elif next_action is Job.TransmissionAction.NODE_TO_NODE:
                    # job is transmitted from one node to another, e.g. when forwarded
                    to_wait = job.get_payload_size() * 8 / self._net_speed_nodes_mbits
                    yield self._env.timeout(actual_time(to_wait))
                    # node1 -> node2 or node2 -> node3
                    if job.get_originator_node_uid() == self._uid or (not job.is_executed() and not job.is_rejected()):
                        # forward the node there
                        destination_node = self._service_discovery.get_node(job.get_forwarded_to())
                        destination_node.schedule_job(job)
                    else:
                        # we are returning from node2 -> node1
                        # re-add to transmission queue of the originator
                        originator_node = self._service_discovery.get_node(job.get_originator_node_uid())
                        originator_node.return_job_to_client(job)

                elif next_action is Job.TransmissionAction.NODE_TO_CLIENT:
                    to_wait = job.get_payload_size() * 8 / self._net_speed_nodes_client_mbits
                    # job returned to client, done
                    yield self._env.timeout(actual_time(to_wait))
                    job.a_done()
                    self._service_data_storage.done_job(job, self._get_reward(job))

                    # check if actions are correct
                    # if DEBUG:
                    #    Log.mdebug(f"{MODULE}#{self._uid}", f"{job} returned to client: {job.get_transmission_actions_list()}")

                else:
                    Log.merr(f"{MODULE}#{self._uid}", f"_process_job_transmission: {job} action not valid")

                if DEBUG:
                    Log.mdebug(f"{MODULE}#{self._uid}",
                               f"_process_job_transmission: end "
                               f"transmitted job={job}, next_action={job.get_transmission_next_action()}, level={self._queued_transmission_jobs.level}")

            except Exception as e:
                traceback.print_exc()
                Log.merr(f"{MODULE}#{self._uid}", f"Node#{self.get_uid()} job transmission process interrupted, e={e}")
                exit(1)

    def _process_jobs_generator(self):
        """Process which generates jobs to dispatch"""

        # checks
        if self._service_discovery is None:
            raise ValueError("Discovery service cannot be None.")

        while True:
            # simulate job arrivals
            try:
                rate_l = self._rate_l

                if self._traffic_model is not None:
                    rate_l = self._traffic_model.get_traffic_at(self._env.now) * self._rate_mu

                if self._rate_l_array is not None:
                    rate_l = self._rate_l_array[self.get_uid()]

                if self._distribution_arrivals == Node.DistributionArrivals.POISSON:
                    yield self._env.timeout(random.expovariate(rate_l))
                elif self._distribution_arrivals == Node.DistributionArrivals.DETERMINISTIC:
                    yield self._env.timeout(1 / rate_l)
                else:
                    raise RuntimeError(f"Arrival policy not valid")

                self._total_jobs += 1

                # decide which job to generate
                rnd = random.random()
                job_type = len(self._job_percentages) - 1
                for i, p in enumerate(self._job_percentages):
                    if rnd <= p:
                        job_type = i
                        break

                # Log.mwarn(self._module(), f"generated job of type = {job_type}, rnd={rnd}, self._job_percentages={self._job_percentages}")

                # generate a job
                job = Job(self._env, node_uid=self.get_uid(), uid=self._total_jobs, end_clb=self._clb_job_end,
                          eps=self._epsilon, payload_size_mbytes=self._job_payload_sizes_mbytes[job_type],
                          duration=self._job_durations[job_type],
                          duration_std_dev=self._job_duration_std_devs[job_type],
                          job_type=job_type, deadline=self._job_deadlines[job_type])

                if DEBUG:
                    Log.mdebug(self._module(),
                               f"_process_jobs_generator: generated j={job}, now={self._env.now}, episode={self._current_episode_number}, rate_l={rate_l}, type={job_type}")

                # append the job to the backlog list
                job.set_episode(self._current_episode_number)
                self._scheduled_jobs.append(job)

                # check if episode ended
                episode_end = (self._total_jobs - self._last_episode_end_at) % self._episode_length == 0
                if episode_end:
                    if DEBUG:
                        Log.minfo(self._module(),
                                  f"_process_jobs_generator: j=#{job}, episode #{self._current_episode_number}, limit={episode_end}")

                    job.set_last_of_episode(True)

                    self._current_episode_number += 1
                    self._last_episode_end_at = self._total_jobs

                    # increase epsilon
                    if self._eps_dynamic and self._epsilon > self._epsilon_min:
                        self._epsilon *= self._epsilon_decay

                # add to the transmission queue
                self._job_transmit(job)

            except simpy.Interrupt:
                Log.mwarn(self._module(), "Job generator interrupted")
                # raise RuntimeError(self._module() + ": Interrupted")
                exit(1)

    def _process_logger_impl(self):
        """Process used for logging values at specific times"""

        if self._session_learning_type is not Node.LearningType.D_SARSA:
            return

        if DEBUG:
            Log.mdebug(f"{MODULE}#{self._uid}", f"_process_logger started")

        while True:
            try:
                # wait 1 second
                yield self._env.timeout(100)

                # log q value every second
                def log_state_data(state, state_string):
                    q_values = [self._d_sarsa_q(state, a) for a in self._get_actions()]
                    for action, value in enumerate(q_values):
                        self._service_data_storage.log_q_value_at_time(int(self._env.now), state_string, self._uid,
                                                                       action, value)

                # log to db
                all_possible_states = self._get_all_possible_states_str()
                for state in all_possible_states:
                    log_state_data([int(c) for c in state], state)


            except Exception as e:
                traceback.print_exc()
                Log.merr(f"{MODULE}#{self._uid}", f"Node#{self.get_uid()} _process_logger interrupted, e={e}")
                exit(1)

    #
    # Internals
    #

    # noinspection PyProtectedMember
    def _job_compute_duration(self, job):
        # compute the job duration given the machine speed
        job_duration = job.get_job_duration() * (1 / self._machine_speed)

        # choose the duration distribution
        if self._job_duration_type == Job.DurationType.GAUSSIAN:
            actual_job_duration = -1.0
            # loop until we get non-negative durations
            while actual_job_duration <= 0.0:
                actual_job_duration = random.gauss(job_duration, pow(job.get_job_duration_std_dev(), 2))
            return actual_job_duration
        elif self._job_duration_type == Job.DurationType.EXPONENTIAL:
            return random.expovariate(1 / job_duration)
        else:
            return job_duration

    def _job_forward_to(self, job, to_node_uid):
        job.set_transmission_next_action(Job.TransmissionAction.NODE_TO_NODE)
        job.a_forwarded(to_node_uid)
        self._job_transmit(job)

    def _job_probe(self, job):
        """Schedule a job for probing"""
        job.a_probing()
        # add the job to probing queue
        self._jobs_probing_list.append(job)
        self._queued_probe_jobs.put(1)

    def _job_transmit(self, job):
        """Schedule a job for transmission, direction is written in job.next_action"""
        self._jobs_transmission_list.append(job)
        self._queued_transmission_jobs.put(1)

    def _job_schedule(self, job):
        """Schedule a job internally in the node"""
        # if node is full loaded
        if self.get_load() >= self._max_jobs_in_queue:
            self._job_reject(job)
            return False

        self._job_accept(job)
        return True

    def _job_accept(self, job: Job):
        self._jobs_list.append(job)
        self._queued_jobs.put(1)

        job.a_in_queue()

    def _job_reject(self, job):
        job.a_rejected()
        self._job_transmit(job)

    def _job_first_dispatching(self, job):
        """Execute the first dispatching action after the job arrived"""
        # get state snapshot
        state = self._get_state_representation(job)

        # make the decision
        chosen_action = self._act(state, job)
        job.save_state_snapshot(state)
        job.save_action(chosen_action)

        # mark as dispatched
        job.a_dispatched()

        # act
        self._act_execute(chosen_action, job)

    #
    # Core
    #

    def _act(self, state, job: Job) -> int or None:
        if self._session_learning_type == Node.LearningType.NO_LEARNING:
            return self._act_no_learning(state, job)
        elif self._session_learning_type == Node.LearningType.Q_DNN:
            pass
            # return self._act_dnnq(state, job)
        elif self._session_learning_type == Node.LearningType.Q_TABLE:
            pass
            # return self._act_q_table(state, job)
        elif self._session_learning_type == Node.LearningType.D_SARSA:
            return self._act_d_sarsa(state, job)

    # noinspection PyMethodMayBeStatic
    def _get_actions(self):
        """Retrieve all the possible actions"""
        if self._actions_space is Node.ActionsSpace.LOCAL_AND_PROBE:
            return [Node.ACTION_EXECUTE_LOCALLY, Node.ACTION_PROBE]  # execute local or probe
        elif self._actions_space is Node.ActionsSpace.FORWARD_TO_AND_PROBE:
            a = [Node.ACTION_EXECUTE_LOCALLY, Node.ACTION_PROBE]
            for i in range(self._n_nodes):
                a.append(2 + i)
            return a
        elif self._actions_space is Node.ActionsSpace.REJECT_LOCAL_AND_FORWARD_TO:
            a = [Node.ACTION_REJECT, Node.ACTION_EXECUTE_LOCALLY]
            for i in range(self._n_nodes):
                a.append(2 + i)
            return a
        else:
            Log.merr(f"{MODULE}#{self._uid}", "ActionsSpace not valid")
            raise RuntimeError("ActionsSpace not valid")

    def _get_possible_actions(self, job: Job, state: List[int]):
        """Returns the possible actions given the state"""
        if self._actions_space is Node.ActionsSpace.LOCAL_AND_PROBE:
            return self._get_actions()
        elif self._actions_space is Node.ActionsSpace.FORWARD_TO_AND_PROBE:
            a = self._get_actions()
            a.remove(self.get_uid() + 2)
            return a
        elif self._actions_space is Node.ActionsSpace.REJECT_LOCAL_AND_FORWARD_TO:
            a = self._get_actions()
            a.remove(self.get_uid() + 2)
            return a
        else:
            Log.mfatal(f"{MODULE}#{self._uid}", "ActionsSpace not valid")

    def _are_no_scheduling_actions(self, job: Job, state: List[int]) -> bool:
        """Returns true if the only action is to reject the job"""
        possible_actions = self._get_possible_actions(job, state)

        if len(possible_actions) == 1 and possible_actions[0] == self._action_size - 1:
            return True
        return False

    def _act_execute_old(self, action, job):
        """Execute the given action for the given job"""
        if action == Node.ACTION_EXECUTE_LOCALLY:
            self._job_schedule(job)

        if action == Node.ACTION_PROBE:
            # probe 1 random node
            random_node = self._service_discovery.get_random_node(current_node_id=self.get_uid())  # type: Node
            # check the load
            if random_node.get_load() < job.get_state_snapshot()[0]:
                job.a_forwarded(random_node.get_uid())
                # schedule it there
                random_node.schedule_job(job)
            else:
                # schedule locally
                self._job_schedule(job)

    def _act_execute(self, action, job):
        """Execute the given action for the given job"""
        if DEBUG:
            Log.mdebug(f"{MODULE}#{self._uid}",
                       f"_act_execute: job={job}, action={'EXECUTE_LOCALLY' if action == 0 else 'PROBE'}")

        if action == Node.ACTION_EXECUTE_LOCALLY:
            job.set_transmission_next_action(Job.TransmissionAction.NODE_TO_CLIENT)
            return self._job_schedule(job)
        elif action == Node.ACTION_PROBE:
            return self._job_probe(job)
        elif action == Node.ACTION_REJECT:
            return self._job_reject(job)
        elif self._actions_space is Node.ActionsSpace.LOCAL_AND_PROBE:
            Log.mfatal(f"{MODULE}#{self._uid}", f"_act_execute: action {action} is not valid for LOCAL_AND_PROBE")

        # here we are at the full action space
        if (self._actions_space is Node.ActionsSpace.FORWARD_TO_AND_PROBE) \
                or (self._actions_space is Node.ActionsSpace.REJECT_LOCAL_AND_FORWARD_TO) \
                and 1 < action <= self._n_nodes + 2 - 1:
            return self._job_forward_to(job, action - 2)
        else:
            Log.mfatal(f"{MODULE}#{self._uid}", f"_act_execute: action {action} is not valid for FORWARD_TO_AND_PROBE")

    #
    # Core -> No learning
    #

    def _no_learning_log_episode(self):
        """Start the ordered memorization and finally the replay dnnq training"""
        # check if replay can start
        if not self._can_replay_start():
            return

        Log.mdebug(f"{MODULE}#{self._uid}",
                   f"_memorize_and_replay_episode: started, len(self._scheduled_jobs)={len(self._scheduled_jobs)}")

        episode = self._scheduled_jobs[0].get_episode()
        episode_jobs = 0
        eps = self._scheduled_jobs[0].get_eps()
        episode_cumulative_reward = 0.0
        i = 0

        # memorize all the experience in order of job scheduling
        while i < len(self._scheduled_jobs) and self._scheduled_jobs[i].get_episode() == episode:
            self._total_processed_job += 1
            episode_jobs += 1

            job = self._scheduled_jobs.pop(i)
            state = job.get_state_snapshot()
            action = job.get_action()

            episode_cumulative_reward += (1 if self._is_job_succeded(job) else 0)  # self._get_reward(job)

            # if job.is_last_of_episode():
            #     reward = episode_cumulative_reward
            # else:
            # reward = self._get_reward(job)

        Log.minfo(self._module(),
                  f"episode={episode} e={eps:.2f} score={episode_cumulative_reward} jobs={episode_jobs}")
        Log.minfo(self._module(), f"processed_jobs={self._total_processed_job} generated_jobs={self._total_jobs} "
                                  f"cur_episode={self._current_episode_number} diff_episode={self._current_episode_number - episode} now={self._env.now}")

        # if self._total_processed_job > self._batch_size:
        self._log_episode_data(episode, eps, episode_cumulative_reward, self._total_processed_job, 0.0, 0.0, 0.0)

    def _act_no_learning(self, state, job):
        """Returns an action choosing from exploration and exploitation"""
        action = None
        load = self._get_total_load_from_state(state)
        possible_actions = self._get_possible_actions(job, state)

        if self._session_no_learning_policy == Node.NoLearningPolicy.RANDOM:
            action = possible_actions[random.randint(0, len(possible_actions) - 1)]

        elif self._session_no_learning_policy == Node.NoLearningPolicy.PWR_2_THRESHOLD:
            # choose by binary policy if one
            if self._pwr2_binary_policy is not None:
                action = self.ACTION_PROBE if self._pwr2_binary_policy[load] == '1' else self.ACTION_EXECUTE_LOCALLY
            else:
                # otherwise by threshold
                action = self.ACTION_PROBE if load >= self._pwr2_threshold else self.ACTION_EXECUTE_LOCALLY

        else:
            Log.merr(f"{MODULE}#{self._uid}", f"Policy {self._session_no_learning_policy} not implemented")
            raise RuntimeError("Policy not implemented")

        return action

    #
    # Core -> Learning -> Sarsa
    #

    def _init_d_sarsa(self):
        if self._use_model_from_session_name is not None:
            model_f = open(
                f"{Node.BASE_DIR_MODELS}/{self._session_learning_type}/{self._use_model_from_session_name}/Node#{self._uid}/d_sarsa.model",
                "rb")
            self._value_function = pickle.load(model_f)
            model_f.close()
        else:
            self._value_function = DSPSarsaTiling(num_tilings=8, max_size=16384, alpha=0.001, beta=0.1)

        Log.minfo(self._module(), f"_init_d_sarsa: {self._value_function}")

    def _d_sarsa_q(self, state, action):
        """Return the Q(S, A, w) value"""
        return self._value_function.value(state + [action])

    def _d_sarsa_learn_episode(self):
        """Learn from experience of an episode"""
        # check if replay can start
        if not self._can_replay_start():
            return

        if DEBUG:
            Log.mdebug(self._module(),
                       f"_d_sarsa_learn_episode: started, len(self._scheduled_jobs)={len(self._scheduled_jobs)}")

        episode = self._scheduled_jobs[0].get_episode()
        episode_jobs = 0
        eps = self._scheduled_jobs[0].get_eps()
        episode_cumulative_reward = 0.0
        last_action = 0
        i = 0

        # process the first job
        job = self._scheduled_jobs.pop(i)
        state = job.get_state_snapshot()
        action = job.get_action()
        reward = self._get_reward(job)
        episode_cumulative_reward += reward
        losses = []

        self._total_processed_job += 1
        episode_jobs += 1

        # memorize all the experience in order of job scheduling
        while len(self._scheduled_jobs) > 0 and self._scheduled_jobs[0].get_episode() == episode:
            job = self._scheduled_jobs.pop(0)
            new_state = job.get_state_snapshot()
            new_action = job.get_action() if not job.is_rejected() else self._action_size - 1

            current_full_state = state + [action]
            new_full_state = new_state + [new_action]

            # update weights
            loss = self._value_function.learn(current_full_state, new_full_state, reward)
            losses.append(abs(loss))

            if DEBUG:
                Log.mdebug(self._module(),
                           f"_d_sarsa_learn_episode: ep={job.get_episode()} jid={job}: state={state}, action={action}, "
                           f"new_state={new_state}, reward={reward}")

            # save current state
            reward = self._get_reward(job)
            state = new_state
            action = new_action

            # update counters
            self._total_processed_job += 1
            episode_jobs += 1
            episode_cumulative_reward += reward

        Log.minfo(self._module(),
                  f"episode={episode} e={eps:.2f} score={episode_cumulative_reward} jobs={episode_jobs} "
                  f"average_reward={self._value_function.stats()} "
                  f"processed_jobs={self._total_processed_job} generated_jobs={self._total_jobs} "
                  f"cur_episode={self._current_episode_number} diff_episode={self._current_episode_number - episode} now={self._env.now}")

        # save the model
        model_f = open(f"{self._DIR_MODELS}/d_sarsa.model", "wb")
        pickle.dump(self._value_function, model_f)
        model_f.close()

        # if self._total_processed_job > self._batch_size:
        self._log_episode_data(episode, eps, episode_cumulative_reward, self._total_processed_job,
                               sum(losses) / float(len(losses)), 0.0, 0.0)

    def _act_d_sarsa(self, state: List[int], job: Job):
        """Given the state as a list of integers and the job, take an action according epsilon and the Q(s,a,w) function"""
        possible_actions = self._get_possible_actions(job, state)

        # check if we need to perform a random action
        if np.random.rand() <= self._epsilon:
            action = possible_actions[random.randrange(len(possible_actions))]
        else:
            values = [(a, self._d_sarsa_q(state, a)) for a in possible_actions]
            max_value = max(values, key=lambda item: item[1])[1]
            max_actions = [action_ for action_, value_ in values if value_ == max_value]

            if DEBUG:
                Log.mdebug(self._module(),
                           f"_act_d_sarsa: j={job} state={state} values={values}, max={max_value}, max_actions={max_actions}")
            # Log.mdebug(self._module(), f"_act_d_sarsa: jid={job.get_id()} weights={self._weights}")
            action = np.random.choice(max_actions)

        if DEBUG:
            Log.mdebug(self._module(),
                       f"_act_d_sarsa: j={job} ep={job.get_episode()} state={state} possible_actions={possible_actions} chosen_action={action}")

        return action

    #
    # Callbacks
    #

    def _clb_job_end(self, job):
        """Called when a job ends, executed or rejected"""
        if DEBUG:
            Log.mdebug(self._module(), f"_clb_job_end: j={job}, episode #{job.get_episode()}: "
                                       f"end: deadline={job.get_deadline()} total_time={job.get_total_time():.2f} "
                                       f"executed={job.is_executed()} rejected={job.is_rejected()} reward={self._get_reward(job)}")

        if self._session_learning_type == Node.LearningType.Q_DNN:
            # replay and memorize, if possible
            # self._dnnq_memorize_and_replay_episode()
            pass
        elif self._session_learning_type == Node.LearningType.Q_TABLE:
            # self._q_table_learn_episode()
            pass
        elif self._session_learning_type == Node.LearningType.D_SARSA:
            self._d_sarsa_learn_episode()
        else:
            self._no_learning_log_episode()

    #
    # Utils
    #

    def _get_state_representation(self, arrived_job: Job) -> List[int]:
        """Returns the current state used for learning"""
        if self._state_type == Node.StateType.ONLY_NUMBER:
            return [self.get_load(), arrived_job.get_type()]
        elif self._state_type == Node.StateType.JOB_TYPE:
            counters = [0 for _ in range(self._job_types)]

            for job in self._jobs_list:
                counters[job.get_type()] += 1

            for job in self._currently_executing_jobs_list:
                counters[job.get_type()] += 1

            counters.append(arrived_job.get_type())
            return counters

        else:
            raise ValueError(f"self._state_type is not valid: {self._state_type}")

    def _get_total_load_from_state(self, state):
        """Returns the total load from a given state according to the state representation"""
        if self._state_type == Node.StateType.ONLY_NUMBER:
            return state[0]
        elif self._state_type == Node.StateType.JOB_TYPE:
            load = 0
            for i in range(self._job_types):
                load += state[i]
            return load
        else:
            raise ValueError(f"self._state_type is not valid: {self._state_type}")

    def _get_all_possible_states_str(self):
        """Returns an array of strings with all the possible states"""
        if self._all_possible_states is not None:
            return self._all_possible_states

        out = []
        if self._state_type == Node.StateType.ONLY_NUMBER:
            for i in range(self._max_jobs_in_queue):
                for j in range(self._job_types):
                    out.append(f"{i}{j}")

        elif self._state_type == Node.StateType.JOB_TYPE:
            def list_rec(state_i, string):
                out = []
                if state_i == self._job_types + 1:
                    return string

                if state_i == self._job_types:
                    for i in range(self._job_types):
                        next_str = string + str(i)
                        out.append(list_rec(state_i + 1, next_str))
                    return out

                for i in range(self._max_jobs_in_queue):
                    next_str = string + str(i)
                    new_str_arr = list_rec(state_i + 1, next_str)

                    # add to output only if states sum to max_jobs_in_queue
                    for new_str in new_str_arr:
                        state_sum = 0
                        for j in range(self._job_types):
                            state_sum += int(new_str[j])
                        if state_sum <= self._max_jobs_in_queue:
                            out.append(new_str)

                return out

            all_states = list_rec(0, "")
            return all_states

        else:
            raise ValueError(f"self._state_type is not valid: {self._state_type}")

        return out

    def _is_episode_over(self, job: Job, state: List[int]):
        """Return if episode is over"""
        # return len(self._get_actions(job, state)) == 0
        return self._total_jobs % self._episode_length == 0

    def _can_replay_start(self):
        """Check if replay and memoization can start"""
        # Log.mdebug(self._module(), f"_can_replay_start: called, len(self._scheduled_jobs)={len(self._scheduled_jobs)}")
        if len(self._scheduled_jobs) == 0:
            return False

        last_job_executed = False
        episode = self._scheduled_jobs[0].get_episode()
        i = 0
        while i < len(self._scheduled_jobs) and self._scheduled_jobs[i].get_episode() == episode:
            job = self._scheduled_jobs[i]
            if self.get_uid() == 0:
                if DEBUG:
                    Log.mdebug(self._module(), f"_can_replay_start: id={job} ep={job.get_episode()} "
                                               f"is_done={job.is_done()} is_last={job.is_last_of_episode()} "
                                               f"rej={job.is_rejected()} action={job.get_action()} "
                                               f"total_time={job.get_total_time():.4f} "
                                               f"forwarded_to={job.get_forwarded_to()} probing_time={job.get_probing_time():.4f} "
                                               f"now={self._env.now:.4f} time_gen={job._time_generated:.4f} "
                                               f"time_queued={job.get_queue_time():.4f} next_action={job.get_transmission_next_action()} "
                                               f"dispatched_time={job.get_dispatched_time():.4f} dispatched={job.is_dispatched()}")
            if not job.is_done():
                return False

            # check if the last job has been executed
            if job.is_last_of_episode():
                last_job_executed = True

            i += 1

        return last_job_executed

    def _is_job_succeded(self, job: Job):
        return job.is_executed() and not job.is_over_deadline() and not job.is_rejected()

    def _get_reward(self, job: Job):
        """Get the reward for the action or for the episode if it is over"""
        if not job.is_done():
            raise RuntimeError(
                "You are requesting the reward from a job that has not returned to the client, please check your code!")

        return 1 if self._is_job_succeded(job) else 0

    def _get_nodes_state(self):
        return [node.get_load() for node in self._service_discovery.get_all_nodes()]

    #
    # Logging
    #

    def _log_episode_data(self, episode, eps, score, total_jobs, loss, mse, mae):
        # self._data_log.append([episode, eps, score, total_jobs, loss, mse, mae])
        self._service_data_storage.done_episode(self._uid, episode, eps, score, total_jobs, loss, mse, mae)

        if self._session_learning_type == Node.LearningType.D_SARSA and self._current_episode_number % 500 == 0:
            def log_state_data(state, state_string, at_time=False):
                q_values = [self._d_sarsa_q(state, a) for a in self._get_actions()]
                for action, value in enumerate(q_values):
                    self._service_data_storage.log_q_value(state_string, self._uid, episode, action, value)

            # log q to db
            all_possible_states = self._get_all_possible_states_str()
            for state in all_possible_states:
                log_state_data([int(c) for c in state], state)

            # self._data_log[self.LOG_KEY_EPISODE].append(episode)
            # self._data_log[self.LOG_KEY_EPS].append(eps)
            # self._data_log[self.LOG_KEY_SCORE].append(score)
            # self._data_log[self.LOG_KEY_TOTAL_JOBS].append(total_jobs)
            # self._data_log[self.LOG_KEY_LOSS].append(loss)
            # self._data_log[self.LOG_KEY_MSE].append(mse)
            # self._data_log[self.LOG_KEY_MAE].append(mae)

        # log_file_path = self._DIR_LOG + "/log-metrics.txt"
        # log_file_fp = open(log_file_path, "a")
        # print("{} {:.3f} {} {} {:.6f} {:.6f} {:.6f}".format(episode, eps, score, total_jobs, loss, mse, mae),
        #       file=log_file_fp)
        # log_file_fp.close()

    def _module(self):
        return f"{self.MODULE}#{self.get_uid()}"

    #
    # Exported methods
    #

    def set_service_discovery(self, service_discovery):
        self._service_discovery = service_discovery

    def set_service_data_storage(self, service_data_storage):
        self._service_data_storage = service_data_storage

    def is_idle(self):
        if DEBUG:
            Log.mdebug(f"{MODULE}#{self._uid}",
                       f"is_idle: id={self.get_uid()} id={id(self)} idle={len(self._currently_executing_jobs_list) == 0} level={self._queued_jobs.level} "
                       f"list={len(self._jobs_list)} time={self._env.now:.4f} level_id={id(self._queued_jobs)}")
        return len(self._currently_executing_jobs_list) == 0 and self._queued_jobs.level == 0 and len(
            self._jobs_list) == 0

    def schedule_job(self, job):
        """Schedule a job from external, without adding to the transmission queue, since only who sends is in charge
        of the transmission"""
        if DEBUG:
            Log.mdebug(f"{MODULE}#{self._uid}", f"schedule_job: scheduling j={job}")

        # job comes from external
        if job.get_originator_node_uid() is not self.get_uid():
            job.set_transmission_next_action(Job.TransmissionAction.NODE_TO_NODE)

        return self._job_schedule(job)

    def return_job_to_client(self, job):
        """Re-add to transmission queue for re-trasmit the node to the client. This is called after a job is executed
        remotely"""
        job.set_transmission_next_action(Job.TransmissionAction.NODE_TO_CLIENT)
        self._job_transmit(job)

    def is_running_job(self):
        return len(self._currently_executing_jobs_list) > 0

    def get_load(self) -> int:
        """Retrieve the number of queued tasks"""
        return self._queued_jobs.level + len(self._currently_executing_jobs_list)

    def get_uid(self):
        """Return the identifier number of the node"""
        return self._uid

    def get_job_list(self):
        return self._jobs_list

    def get_max_queue_length(self):
        return self._max_jobs_in_queue
