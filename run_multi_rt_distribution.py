#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.
from datetime import datetime

from node import Node
from simulate import Simulation

NUMBER_OF_STEPS = 10

SIMULATION_TIME = 2000
NODES = 6  # number of nodes
SESSION_ID = datetime.now().strftime("%Y%m%d-%H%M%S")

LEARNING_TYPE = Node.LearningType.D_SARSA
NO_LEARNING_POLICY = Node.NoLearningPolicy.PWR_2_THRESHOLD

for i in range(NUMBER_OF_STEPS + 1):
    distribution_rt = round(1 / NUMBER_OF_STEPS * i, 2)
    distribution_nrt = round(1 - distribution_rt, 2)

    print(f"Selected distribution is: rt/nrt={distribution_rt}/{distribution_nrt}")

    # run the simulation
    Simulation.simulate(SIMULATION_TIME, NODES, f"{SESSION_ID}-{i}", LEARNING_TYPE, NO_LEARNING_POLICY, distribution_rt)
