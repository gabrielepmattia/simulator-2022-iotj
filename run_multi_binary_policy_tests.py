#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.
#
#   All rights reserved.

from __future__ import annotations

import os
import signal
import sqlite3
import sys
from datetime import datetime

import simpy

from job import Job
from log import Log
from node import Node
from service_data_storage import ServiceDataStorage
from service_discovery import ServiceDiscovery

"""
Run the simulation of deadline scheduling
"""

MODULE = "RunMultiBinaryPolicyTests"

LOG_DIR = "_log/no-learning/PWR_2_THRESHOLD/_MULTI_RUNS"

MAX_JOBS_IN_QUEUE = 5


class Simulation:
    SIMULATION_TIME = 2500
    SIMULATION_TOTAL_TIME = SIMULATION_TIME + 50
    NODES = 6  # number of nodes

    LEARNING_TYPE = Node.LearningType.NO_LEARNING
    NO_LEARNING_POLICY = Node.NoLearningPolicy.PWR_2_THRESHOLD

    def __init__(self, env, policy_str=None):
        self._env = env
        self._session_id = datetime.now().strftime("%Y%m%d-%H%M%S")
        self._policy_str = policy_str

        if env is None:
            raise RuntimeError("Env cannot be none")

        if policy_str is None:
            raise RuntimeError("Policy cannot be none")

    def simulate(self):
        nodes = []
        # create nodes

        for i in range(Simulation.NODES):
            job_duration = 0.020
            nodes.append(Node(self._env,
                              i,
                              self._session_id,
                              simulation_time=Simulation.SIMULATION_TIME,
                              # rates
                              rate_l=(1 / job_duration) * 0.75,
                              rate_mu=1 / job_duration,
                              rate_mu_realtime=1 / job_duration,
                              # job info
                              job_realtime_percentage=0.0,
                              job_realtime_deadline=0.043,
                              job_deadline=0.043,
                              job_duration_type=Job.DurationType.FIXED,
                              job_payload_size_mbytes=0.1,
                              # node info
                              max_jobs_in_queue=MAX_JOBS_IN_QUEUE,
                              distribution_arrivals=Node.DistributionArrivals.POISSON,
                              delay_probing=0.005,
                              # learning
                              learning_type=Simulation.LEARNING_TYPE,
                              no_learning_policy=Simulation.NO_LEARNING_POLICY,
                              pwr2_binary_policy=self._policy_str,
                              # threshold=6,
                              # use_model_from_session_name="20210412-125752",
                              episode_length=20,
                              eps=0.9,
                              eps_dynamic=True,
                              eps_min=0.05,
                              # other
                              skip_plots=True
                              ))

        # add them discovery service
        discovery = ServiceDiscovery(nodes)
        data_storage = ServiceDataStorage(nodes, self._session_id, Simulation.LEARNING_TYPE, Simulation.NO_LEARNING_POLICY)

        # init nodes services, and data
        for node in nodes:
            node.set_service_discovery(discovery)
            node.set_service_data_storage(data_storage)
        for node in nodes:
            node.init()

        Log.minfo(MODULE, "Started simulation")
        self._env.run(until=Simulation.SIMULATION_TOTAL_TIME)
        Log.minfo(MODULE, "Simulation ended")

        data_storage.done_simulation()

        return data_storage.get_log_dir()


def main(argv):
    session_id = datetime.now().strftime("%Y%m%d-%H%M%S")
    os.makedirs(f"{LOG_DIR}", exist_ok=True)
    db = init_db(session_id)

    # execute simulation for every possible policy
    def i_to_policy(i):
        policy_str = str(bin(i))[2:]
        for _ in range(MAX_JOBS_IN_QUEUE + 1 - len(policy_str)):
            policy_str = '0' + policy_str
        return policy_str

    for i in range(pow(2, MAX_JOBS_IN_QUEUE + 1)):
        policy_str = i_to_policy(i)
        Log.minfo(MODULE, f"Simulating policy {i}={policy_str}")

        sim = Simulation(simpy.Environment(), policy_str)
        simulation_log_dir = sim.simulate()

        save_stat(db, i, policy_str, simulation_log_dir)

        os.makedirs(f"{simulation_log_dir}/{i}-{policy_str}", exist_ok=True)

    db.close()


def init_db(session_id):
    db = sqlite3.connect(f"{LOG_DIR}/{session_id}.db")
    cur = db.cursor()

    cur.execute(f'''CREATE TABLE simulations (
                        i integer,
                        policy text,
                        total_reward integer,
                        total_executed integer,
                        total_rejected integer,
                        total_jobs integer,
                        total_forwarded integer,
                        avg_time_total real,
                        primary key (i, policy)
    )''')
    db.commit()
    cur.close()

    return db


def save_stat(db, i, policy_str, simulation_log_dir):
    Log.minfo(MODULE, f"save_stat: from {simulation_log_dir}/log.db")

    db_simulation = sqlite3.connect(f"{simulation_log_dir}/log.db")
    db_simulation_cur = db_simulation.cursor()

    total_reward = 0
    total_executed = 0
    total_rejected = 0
    total_forwarded = 0
    total_jobs = 0
    avg_time_total = 0.0

    # get total reward
    res = db_simulation_cur.execute("select sum(reward) from jobs")
    for line in res:
        total_reward = line[0]

    # get total executed
    res = db_simulation_cur.execute("select count(*) from jobs where executed = 1")
    for line in res:
        total_executed = line[0]

    # get total rejected
    res = db_simulation_cur.execute("select count(*) from jobs where rejected = 1")
    for line in res:
        total_rejected = line[0]

    # get total forwarded
    res = db_simulation_cur.execute("select count(*) from jobs where node_forwarded_to_uid > -1")
    for line in res:
        total_forwarded = line[0]

    # get total jobs
    res = db_simulation_cur.execute("select count(*) from jobs")
    for line in res:
        total_jobs = line[0]

    # get total jobs
    res = db_simulation_cur.execute("select avg(time_total) from jobs where executed = 1")
    for line in res:
        avg_time_total = line[0]

    cur = db.cursor()
    cur.execute(f'''insert into simulations values (
                    {i},
                    "{policy_str}",
                    {total_reward},
                    {total_executed},
                    {total_rejected},
                    {total_jobs},
                    {total_forwarded},
                    {avg_time_total}
                )''')

    db.commit()
    cur.close()


#
# Signals
#

def signal_handler(signal, frame):
    Log.minfo(MODULE, "Interrupt received, closing gracefully")
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

#
# Entrypoint
#

if __name__ == "__main__":
    main(sys.argv)
