#  simulator-2022-iotj - Simpy simulator of online scheduling between edge nodes
#  Copyright (c) 2021 - 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   All rights reserved.
#
#   All rights reserved.
#
#   All rights reserved.
import math
import os
import sqlite3

import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from log import Log
from node import Node
from utils import Utils

MODULE = "PlotDynamicLoad"

N_NODES = 12
ACTIONS = Node.ActionsSpace.FORWARD_TO_AND_PROBE

DB_FILE_FOLDER = "20220414-122333"
DB_FILE = f"./_log/learning/D_SARSA/{DB_FILE_FOLDER}/log.db"
TRAFFIC_MODEL_FILE = lambda i: f"./traffic/city/data/traffic_node_{i}.csv"
# TRAFFIC_MODEL_FILE = "./traffic/namex/namex-traffic-daily-20210420.csv"
# TRAFFIC_MODEL_FILE = "./traffic/fictious/fictious_1.csv"
TRAFFIC_MODEL_CYCLES = 3
TRAFFIC_MODEL_SHIFT = 0  # 1200
TRAFFIC_MODEL_MAX_PARSED = None
TRAFFIC_MODEL_STEADY = False
TRAFFIC_MODEL_STEADY_FOR = 2000
TRAFFIC_MODEL_STEADY_EVERY = 2000
SELECTED_NODE = 5

db = sqlite3.connect(DB_FILE)
cur = db.cursor()

simulation_time = 0

os.makedirs("./_plots", exist_ok=True)

#
# retrieve simulation time
#

res = cur.execute("select max(generated_at) from jobs")
for line in res:
    simulation_time = math.ceil(line[0])
Log.mdebug(MODULE, f"simulation_time = {simulation_time}")

#
# retrieve states
#

states = []
res = cur.execute("select state from q_values group by state order by state")
for line in res:
    states.append(line[0])
Log.mdebug(MODULE, f"states={states}")

#
# retrieve states
#

job_types = 0
res = cur.execute("select max(type) from jobs")
for line in res:
    job_types = line[0] + 1
Log.mdebug(MODULE, f"job_types={job_types}")

#
# do a heatmap for every job type
#

mats = []
y_ticks_arr = []


def do_heatmap(job_type):
    Log.mdebug(MODULE, f"do_heatmap: job_type={job_type}")

    usable_states = list(filter(lambda x: x[-1] == str(job_type), states))
    Log.mdebug(MODULE, f"do_heatmap: usable_states={usable_states} ({len(usable_states)})")

    for node_i in range(N_NODES):
        Log.mdebug(MODULE, f"do_heatmap: computing node_i={node_i}")

        mat = [[0] for _ in usable_states]
        x_ticks = [0]
        y_ticks = usable_states

        # print(mat)

        res = cur.execute(f'''select time, state, action, max(value) 
                                from q_values_by_time where node_uid = {node_i} and action != {node_i + 2} and state LIKE "%{job_type}"
                                group by time, state 
                                order by time, state''')
        current_t = -1
        state_i = 0

        for line in res:
            t = line[0]
            Log.mdebug(MODULE, f"do_heatmap: t={t}")

            if t % 100 != 0 and t != 0:
                continue

            if t != current_t:
                x_ticks.append(t if t % 1000 == 0 or t == 0 else "")
                state_i = 0
                current_t = t

            mat[state_i].append(line[2])
            state_i += 1

        # print(mat)
        # print(len(mat))
        # print(len(states))

        # mat = [mat[i] for i in filter(lambda x: x % 2 == 1, list(range(len(mat))))]
        y_ticks = usable_states  # [y_ticks[i][0] for i in filter(lambda x: x[-1] == str(job_type), list(range(len(y_ticks))))]

        mats.append(mat)
        y_ticks_arr.append(y_ticks)


do_heatmap(0)

fig, ax = plt.subplots(nrows=N_NODES, ncols=1)

cmap = plt.get_cmap("Accent", 2 + (N_NODES if ACTIONS is Node.ActionsSpace.FORWARD_TO_AND_PROBE else 0))

for i in range(N_NODES):
    im = ax[i].imshow(mats[i], cmap=cmap)
    ax[i].set_xticklabels([])

    ax[i].set_yticks(list(range(len(y_ticks_arr[i]))))
    ax[i].set_yticklabels(y_ticks_arr[i], fontsize=6)

    # plt.colorbar(im, ax=ax[i, j])

    if i == 0:
        # colorbar
        actions = ["Local", "Probe Random"]
        if ACTIONS is Node.ActionsSpace.FORWARD_TO_AND_PROBE:
            for i in range(N_NODES):
                actions.append(fr"To Node\#{i}")

        color_bar = False
        fmt = matplotlib.ticker.FuncFormatter(lambda x, pos: actions[int(pos)])

        # im = ax[0].imshow(mat, cmap=cmap)
        # print(mat)
        if color_bar:
            divider = make_axes_locatable(ax[0])
            cax = divider.new_vertical(size=0.15, pad=0.4)
            fig.add_axes(cax)
            # fig.colorbar(im, cax = cax, orientation = 'horizontal')

            bounds = list(range(len(actions) + 1))
            # print(f"cbar_tags_bounds={bounds}")

            tick_length = ((len(actions) - 1) / (len(actions)))
            ticks = [tick_length / 2 + tick_length * i for i in range(len(actions))]
            cbar = ax[0].figure.colorbar(im, cax=cax, orientation="horizontal", format=fmt)  # ax=ax[0],
            cbar.ax.set_ylabel("Action")
            cbar.set_ticks(ticks)
            print(f"cbar_ticks={ticks}")
            print(f"cbar_actions={actions}")
            # tick_texts = cbar.ax.set_yticklabels(actions)

fig.tight_layout(h_pad=0)
fig.set_figwidth(6.4)  # 6.4
fig.set_figheight(9)  # 4.8
fig.subplots_adjust(wspace=0, hspace=0.1)
# fig.tight_layout(h_pad=0)

plt.savefig(f"_plots/plot_dynamic_policy_stacked-{DB_FILE_FOLDER}_{Utils.current_time_string()}.pdf")

exit(0)

"""
#
# retrieve traffic data
#

t_models = []
for i in range(N_NODES):
    t_model = TrafficModel(raw_path=TRAFFIC_MODEL_FILE(i),
                           cycles=TRAFFIC_MODEL_CYCLES,
                           shift=TRAFFIC_MODEL_SHIFT * i,
                           max_x=simulation_time,
                           parsed_x_limit=TRAFFIC_MODEL_MAX_PARSED,
                           steady=TRAFFIC_MODEL_STEADY,
                           steady_for=TRAFFIC_MODEL_STEADY_FOR,
                           steady_every=TRAFFIC_MODEL_STEADY_EVERY)
    t_models.append(t_model)

x_traffic = []
y_traffic = []
for i in range(N_NODES):
    x_traffic.append([])
    y_traffic.append([])
    for t in range(simulation_time):
        x_traffic[i].append(t)
        y_traffic[i].append(t_models[i].get_traffic_at(t))

#
# retrieve reward data
#

x_reward = []
y_reward = []

res = cur.execute(f"select cast(generated_at as integer), avg(reward) from jobs where node_uid = {SELECTED_NODE} group by cast(generated_at as integer)")
average_every_secs = 30
last_sec = -1
avg_reward = 0.0
avg = 0.0
added = 0
for line in res:
    t = line[0]
    avg_reward = line[1]

    avg += avg_reward
    added += 1

    if t % average_every_secs == 0 and t > 0:
        # print(f"t={t}, added={added}, avg={avg / added}")
        x_reward.append(t)
        y_reward.append(avg / added)
        avg = 0.0
        added = 0

# no learning reward
x_n_reward = []
y_n_reward = []

db2 = sqlite3.connect("./_log/no-learning/PWR_2_THRESHOLD/20210531-151100/log.db")
cur2 = db2.cursor()

res2 = cur2.execute(f"select cast(generated_at as integer), avg(reward) from jobs where node_uid = {SELECTED_NODE} group by cast(generated_at as integer)")
last_sec = -1
avg_reward = 0.0
avg = 0.0
added = 0
for line in res2:
    t = line[0]
    avg_reward = line[1]

    avg += avg_reward
    added += 1

    if t % average_every_secs == 0 and t > 0:
        # print(f"t={t}, added={added}, avg={avg / added}")
        x_n_reward.append(t)
        y_n_reward.append(avg / added)
        avg = 0.0
        added = 0

# print(x_n_reward)
# print(y_n_reward)
# exit(0)

#
# retrieve eps data
#

x_eps = [0]
y_eps = [1.0]

res = cur.execute(f"select cast(generated_at as integer), avg(eps) from jobs where node_uid = {SELECTED_NODE} group by cast(generated_at as integer)")
average_every_secs = 100
last_sec = -1
avg_reward = 0.0
avg = 0.0
added = 0
for line in res:
    t = line[0]
    avg_reward = line[1]

    avg += avg_reward
    added += 1

    if t % average_every_secs == 0 and t > 0:
        x_eps.append(t)
        y_eps.append(avg / added)
        avg = 0.0
        added = 0

#
# plot
#


PlotUtils.use_tex()

cmap = plt.get_cmap("Accent", 2 + (N_NODES if ACTIONS is Node.ActionsSpace.FORWARD_TO_AND_PROBE else 0))
cmap_load = cmap if ACTIONS is Node.ActionsSpace.FORWARD_TO_AND_PROBE else plt.get_cmap("Accent", 2 + N_NODES)
cmap_def = plt.get_cmap("tab10")

plt.clf()

# fig, ax = plt.subplots()
fig, ax = plt.subplots(nrows=3, ncols=1)

# colorbar
actions = ["Local", "Probe Random"]
if ACTIONS is Node.ActionsSpace.FORWARD_TO_AND_PROBE:
    for i in range(N_NODES):
        actions.append(fr"To Node\#{i}")

color_bar = True
fmt = matplotlib.ticker.FuncFormatter(lambda x, pos: actions[int(pos)])

im = ax[0].imshow(mat, cmap=cmap)
# print(mat)

divider = make_axes_locatable(ax[0])
cax = divider.new_vertical(size=0.15, pad=0.4)
fig.add_axes(cax)
# fig.colorbar(im, cax = cax, orientation = 'horizontal')

bounds = list(range(len(actions) + 1))
# print(f"cbar_tags_bounds={bounds}")
if color_bar:
    tick_length = ((len(actions) - 1) / (len(actions)))
    ticks = [tick_length / 2 + tick_length * i for i in range(len(actions))]
    cbar = ax[0].figure.colorbar(im, cax=cax, orientation="horizontal", format=fmt)  # ax=ax[0],
    cbar.ax.set_ylabel("Action")
    cbar.set_ticks(ticks)
    print(f"cbar_ticks={ticks}")
    print(f"cbar_actions={actions}")
    # tick_texts = cbar.ax.set_yticklabels(actions)

ax[0].set_xticks(list(range(len(x_ticks))))
ax[0].set_yticks(list(range(len(y_ticks))))
ax[0].set_xticklabels([])
# ax[0].set_xticklabels(x_ticks, rotation=30, ha="right", rotation_mode="anchor")
ax[0].set_yticklabels(y_ticks)
ax[0].set_ylabel("State")
# ax[0].set_xlabel("Time")

# plot_traffic
lines = []
for i in range(N_NODES):
    line, = ax[1].plot(x_traffic[i], y_traffic[i], color=cmap_load(i + 2))
    lines.append(line)

    print(y_traffic[i][:10])

ax[1].margins(0)
ax[1].set_ylim([0, 1])
ax[1].set_ylabel(r"$\rho$")
ax[1].set_xticklabels([])
# ax[1].set_xlabel("Time")
ax[1].legend([lines[i] for i in range(N_NODES)], [fr"To Node\#{i}" for i in range(N_NODES)], fontsize="x-small")

# plot reward
ax2_bis = ax[2].twinx()

ax[2].plot(x_reward, y_reward, color=cmap_def(4))
ax[2].plot(x_n_reward, y_n_reward, color=cmap_def(5))
ax[2].margins(0)
ax[2].set_ylim([0.9, 1.0])
ax[2].set_ylabel(r"$\iota$")
ax[2].set_xlabel("Time")

# plot eps
ax2_bis.plot(x_eps, y_eps, color=cmap_def(3))
ax2_bis.set_ylim([0.0, 1.0])
ax2_bis.margins(0)
ax2_bis.set_ylabel(r"$\epsilon$")

# cax.set_title(f"Dynamic Policy and Reward for Node\#{SELECTED_NODE}")
fig.tight_layout(h_pad=0)
fig.set_figwidth(10)  # 6.4
fig.set_figheight(4.8)  # 4.8
fig.subplots_adjust(wspace=0, hspace=0.1)

plt.savefig(f"_plots/heat_policy_dynamic-{DB_FILE_FOLDER}_{Utils.current_time_string()}.pdf")
plt.close(fig)

cur.close()
db.close()

"""

'''
PlotUtils.use_tex()

figure, axis = plt.subplots(nrows=2, ncols=1)
axis[0].set_title(r"Sarsa, 6 states, 6 nodes, deadline = $0.043s$, job\_duration = $0.020s$")
axis[0].plot(x, y_reward)
# axis[0].set_xlabel("Time")
axis[0].set_ylabel("Average Reward")

axis[1].plot(x, y_traffic)
axis[1].set_xlabel("Time")
axis[1].set_ylabel(r"$\rho$")

plt.savefig("_plots/dynamic_plot.pdf")
'''
